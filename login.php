<script>
    function goRegister() {
        //var choice = confirm("Do You wish to Register? (required to Login: Takes 30 Seconds!)");
        //if (choice == true) {
        window.location.assign("<? echo SITEURL ?>https://rejuvenair.app/register");
        //} else {
        // do Nothing
        //}
    }
</script>

<?php $user = "" ?>

<?php
if ($user == "") {
    echo '<script>$(window).bind("load", function() { goRegister(); });</script>'; // add documentReady
}
?>

<style>
    .animate {
        display: grid;
        place-content: center;
        position: relative;
        border: solid 1.5em rgba(239, 239, 239, 0.05);
        padding: 0 1em;

        box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.2), 2px 2px 17px rgba(0, 0, 0, 0.35), 2px 2px 25px rgba(0, 0, 0, 0.5);
        /* background: url("../img/bg2.jpg") 50%/cover border-box padding-box; */

        background-blend-mode: screen;
        color: #fff;
        /* height: 75vh; */
        font-weight: 500 !important;
    }

    .animate:before {
        position: absolute;
        z-index: -1;
        top: -1.5em;
        right: -1.5em;
        bottom: -1.5em;
        left: -1.5em;
        border: inherit;
        border-color: transparent;
        background: inherit;
        background-clip: border-box;
        -webkit-filter: blur(9px);
        filter: blur(9px);
        -webkit-clip-path: inset(0);
        clip-path: inset(0);
        content: "";
    }

    .custom-select {
        position: relative;
        display: inline-block;
        font-family: sans-serif;
        font-size: 93%;
        margin: 30px 50px;
    }

    .custom-select span {
        display: inline-block;
        min-width: 180px;
        color: #FFF;
        background-color: #333;
        padding: 5px;
        cursor: pointer;
    }

    .custom-select ul {
        border: 1px solid #333;
        max-height: 200px;
        overflow: auto;
        z-index: 10000;
    }

    .custom-select ul li {
        padding: 5px;

        cursor: pointer;
    }

    .custom-select ul li ul li:hover {}

    .custom-select ul li:last-child {
        border-bottom: none;
    }

    input,
    button,
    select,
    textarea {
        height: 4.5vh !important;
    }

    .progress-bar {
        position: relative;
        width: 100px;
        height: 100px;
    }

    .circle {
        height: 100%;
        right: 0px;
        position: absolute;
        border: solid 5px #a9a9a9;
        border-top-color: #a9d161;
        border-radius: 50%;
    }

    .border {
        width: 100%;
        transform: rotate(135deg);
        animation: spin 1.3s steps(2) .2s infinite;
        -webkit-animation: spin 1.3s linear infinite;

    }


    @-webkit-keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }
</style>
<div class="contentLoader ng-scope" ng-show="loading" style="display: none;position: fixed; z-index: 9999; margin: 0px auto; width: 100%; text-align: center; height: 100%;">
    <!-- <img src="images/shows-loader.gif"
        style="border-radius: 0px 0px 5px 5px; padding: 10px 15px; position: relative; z-index: 9999;"> -->
</div>
<div class="login-bakg">
    <div class="row" style="margin-top: 2.5vh;">
        <div class="text-center">
            <a href="home">
                <img class="logoAnim" style="width: 9.5vh;max-width: 50%;" src="img/logo.png">
            </a>
        </div>
    </div>
    <div class="row">
        <!-- 

            

<div class="col-md-7">
      </div>
 -->
        <div class="login_container" style="zoom: 0.9;margin-top: 5.5vh">
            <div class="animate form login_form">
                <section class="login_content">
                    <form onsubmit="goLogin()" name="loginForm" method="post" class="form-horizontal" role="form">
                        <div class="clearfix"></div>
                        <!-- 
<ul class="phone_nav">
                        <li class="phone_text">Login as:</li>
                        <li class="phone_no">careworker1</li>
                    </ul>
                    <br>
 -->
                        <h1 style="color: black;">Rejuvenair</h1>
                        <div class="loginBoxesContainer" style="margin-top: 7.5vh;">
                            <!-- <div class="col-md-3">
                                <div class="progress-bar" style="background: transparent;">
                                    <div class="circle border">
                                    </div>
                                </div>
                            </div> -->
                            <div>
                                <select id="cachedFavs" ng-model="selectedItem" ng-options="item.unit_name group by item.unit_location for item in items track by item.unit_id" ng-change="cachedFavs()"></select>
                            </div>
                            <div class="custom-select">
                                <span>Select Unit from Favourites</span>
                            </div>
                            <div>
                                <input id="loginID" type="text" class="form-control" placeholder="User Name" name="username" value="" disallow-spaces ng-model="login.username" required focus autocomplete="off">
                            </div>
                            <div>
                                <input type="password" class="form-control" placeholder="Password" ng-model="login.password" disallow-spaces required>
                            </div>
                            <div>
                                <button style="width:100%" type="submit" class="text-center btnBg btnFg login-button" ng-click="doLogin(login)" data-ng-disabled="loginForm.$invalid">
                                    LOGIN
                                </button>
                                <!-- 
 <button style="width:49.5%" type="submit" class="pull-right btn btn-default login-button" ng-click="doLoginOffline(login)" data-ng-disabled="loginForm.$invalid">
                            LOGIN OFFLINE
                        </button>
 -->
                            </div>
                            <div class="clearfix"></div>
                            <!-- 
<p class="change_link">
                        <a href="#" class="white_color" data-toggle="modal" data-target="#tba">Login as different user</a>            
                    </p>
 -->
                            <div class="resetPasswordContainer" style="margin-top: 1.75rem;padding-left: 0.9rem;">
                                <p class="change_link">
                                    <a href="#" class="white_color" data-toggle="modal" data-target="#resetPassword" style="color: black!important;">
                                        Reset
                                        Password
                                    </a>
                                </p>
                            </div>
                            <!-- 
  <ul class="phone_nav">
                        <li class="phone_text">For Support Issues</li>
                        <li class="phone_no">0208 271 892</li>
                    </ul>
 -->
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</div>
<div class="modal fade " id="resetPassword" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content rest-password padding-bottom20" style="background-color: #4ca1af">
            <div class="modal-header header-border" style="background-color: #4ca1af">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <br>
            </div>
            <div class="modal-body">
                <form class="form-validate form-horizontal" name="forgotForm" method="post" novalidate data-custom-submit="doForgot(forgot)">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <label for="cname" class="control-label" style="width: 100%;padding: 1rem 1rem 1rem 1rem;">
                                    <input style="background: transparent none repeat scroll 0% 0%;" class="form-control rest-password-textfield usename" id="username" name="username" ng-model="forgot.username" type="text" placeholder="User Name" required ng-model-options="{ updateOn: 'blur' }">
                                    <small class="errorMessage" ng-show="submitted && forgotForm.username.$error.required">
                                        This field is
                                        required.
                                    </small>
                                </label>
                                <div class="col-md-2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div id="button_float" style="float: none; text-align: center; margin-top: 20px;">
                                        <button ng-click="submitted=true" class="btn rest-button font-bold" type="submit">Reset Password</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var loginID = '<?= $user ?>';
        console.log("User: " + loginID);
        if (loginID == '') {
            var loginID = localStorage.getItem("loginID");
            document.getElementById("loginID").value = loginID;
        }
    });

    function goLogin() {

        loginID = document.getElementById("loginID").value;
        localStorage.setItem("loginID", loginID);

    }

    //var page = '<?= SITEURL ?>' + 'user/salesprog/';

    function clearDefault() {

        var r = confirm("Click OK to Clear Current Property ID");
        if (r == true) {
            blurt("Success", "Default Progression ID Reset");
            setTimeout(function() {
                localStorage.setItem("salesProgID", "");
                window.location.href = page + "clearDefault";
            }, 1000);
        } else {
            return;
        }
    }

    function autoLoadWindow() {
        document.getElementById('enterPIDButton2').click();
    }

    function refreshBtn() {
        window.location.href = page + pid;
    }
</script>