app.controller('authCtrl', function (
  $scope,
  $timeout,
  $rootScope,
  $window,
  $route,
  $routeParams,
  $location,
  $http
) {
  // Data

  $rootScope.currentLocation = ''
  $scope.currentLocation = ''

  /* add Access Level */

  //  var xmlHttp;
  //
  //     function srvTime() {
  //         try {
  //             //FF, Opera, Safari, Chrome
  //             xmlHttp = new XMLHttpRequest();
  //         }
  //         catch (err1) {
  //             //IE
  //             try {
  //                 xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
  //             }
  //             catch (err2) {
  //                 try {
  //                     xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
  //                 }
  //                 catch (eerr3) {
  //                     //AJAX not supported, use CPU time.
  //                     alert("AJAX not supported");
  //                 }
  //             }
  //         }
  //         xmlHttp.open('HEAD', window.location.href.toString(), false);
  //         xmlHttp.setRequestHeader("Content-Type", "text/html");
  //         xmlHttp.send('');
  //         return xmlHttp.getResponseHeader("Date");
  //     }
  //
  //     var st = srvTime();
  //     var serverDate = new Date(st);
  //     $scope.dtmax = serverDate;
  //
  //     $scope.today = function () {
  //         $scope.dt = serverDate;
  //     };

  //     $scope.today();

  // if ($location.$$host == 'localhost') {
  //   var hostName = $location.$$protocol + '://' + $location.$$host;
  //   var base_url = hostName + '/ourwebsite/';
  // } else {
  //   var base_url = $location.$$protocol + '://' + $location.$$host + '/newsite/';
  // }

  $scope.loadCurrntOrgPageTitle = function (orgPageTitle) {
    $rootScope.orgPageTitle = orgPageTitle
  }

  $scope.loadMainMenuCurrntActiveClass = function (ActiveClass) {
    $scope.loading = true
    $rootScope.activeClass = ActiveClass
    $scope.loading = false
  }

  $scope.selectedItem = ''
  $rootScope.selectedFav = null
  $scope.cachedFavs = function () {
    $rootScope.selectedFav = JSON.stringify($scope.selectedItem)
    console.log('selectedItem: ' + JSON.stringify($scope.selectedItem))

    $scope.selectedFav = $rootScope.favsFromDB
    $rootScope.location = { location_id: $scope.selectedItem.location_id }
  }

  /* FUNCTIONS */

  // for table sorting
  $scope.sort = function (keyname) {
    $scope.sortBy = keyname //set the sortBy to the param passed
    $scope.reverse = !$scope.reverse //if true make it false and vice versa
  }

  $scope.fileReaderSupported = window.FileReader != null
  $scope.profilePhotoChanged = function (files) {
    if (files != null) {
      var file = files[0]
      if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
        $timeout(function () {
          var fileReader = new FileReader()
          fileReader.readAsDataURL(file)
          fileReader.onload = function (e) {
            $timeout(function () {
              $scope.resultData.ProfilePhoto = e.target.result
            })
          }
        })
      }
    }
  }

  var serviceBase = 'api/api.php?request='
  $scope.login = {
    username: 'client1'
  }

  // login request
  $scope.doLogin = function (reqparams) {
    $scope.loading = true

    $rootScope.userLoggedIn = false

    $http({
      method: 'post',
      data: $.param({
        reqparams: reqparams
      }),
      url: serviceBase + 'Login',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function (results) {
      console.log('results: ' + JSON.stringify(results))

      //  console.log('$rootScope.careworkerId: ' + $rootScope.careworkerId);
      if (results.status_code == '0') {
        //                 Data.toast(results);
        Swal.fire({
          icon: 'info',
          title: '<strong>Sorry!</strong>',
          html:
            '<div style="font-size: 1.25rem">Wrong Username or Password</div><br><br><p>Please Try Again or Click Reset Password</p>'
        })
      }

      // Swal.fire({
      // 			  title: '<strong> Search</strong>',
      // 			  icon: 'info',
      // 			  html: '<div style="font-size: 1.25rem">Please See Notes</div>' + '',
      // 			  showCancelButton: true,
      // 			  showConfirmButton: true,
      // 			  useRejections: true,
      // 			  closeOnCancel: true,
      // 			  focusConfirm: false,
      // 			  confirmButtonText: '<i class="fa fa-file-text-o"></i>&nbsp;Client Search/Notes',
      // 			  confirmButtonAriaLabel: 'Client Search/Notes',
      // 			  cancelButtonText: '<i class="fa fa-times"></i>&nbsp;Cancel',
      // 			  cancelButtonAriaLabel: 'Cancel'
      // 			}).then(function(isConfirm) {
      // 				console.log('Value: ' + JSON.stringify(isConfirm));
      // 				if (isConfirm.value && isConfirm.value === true) {
      // 				  location.path('/home/client/search');
      // 				  console.log('You Choose to GoTo Notes');
      // 				} else {
      // 				  console.log('Cancel')
      // 				}
      // 			});
      // 			return;
      // 		}

      if (results.status_code == '0') {
        $route.reload()
      } else {
        $scope.details = results.response_data
        $rootScope.loggedIn = true
        $rootScope.accessLevel = $scope.details.AccessLevel
        console.log('accessLevel: ' + $rootScope.accessLevel)

        $exampleResultsObj = {
          status_code: '1',
          status: 'success',
          message: 'Login Successful',
          fullName: 0,
          response_code: '200',
          response_data: {
            UserID: '2',
            FirstName: 'User',
            LastName: 'One',
            UserName: 'user1',
            EmailID: 'user1@gmail.com',
            StatusID: '1',
            UserAccessID: '2',
            UserType: '1',
            AccessLevel: '1'
          }
        }

        $location.path('home')
      }

      $scope.loading = false
    })
  }

  function isEmpty (obj) {
    return Object.keys(obj).length === 0
  }

  $scope.loadCacheFavs = function () {
    var cachedFavs = localStorage.getItem('cachedFavs')
    console.log('cachedFavs: ' + cachedFavs)

    if (cachedFavs != null) {
      if (isEmpty(cachedFavs) != true) {
        // load cache object to drop menu
        console.log('cachedFavs: ' + JSON.stringify(cachedFavs))

        $scope.items = JSON.parse(cachedFavs)
      } else {
        console.log('cachedFavs Empty ')
        $scope.items = [
          {
            id: 0,
            unit_id: 0,
            location_id: 0,
            unit_name: 'No Favourites',
            unit_location: ''
          }
        ]
      }
    }
  }

  $scope.loadCacheFavs()

  // doForgot request
  $scope.doForgot = function (reqparams) {
    $('.rest-button').addClass('disabled')
    $('.rest-button').text('Sending...')
    $http({
      method: 'post',
      data: $.param({
        reqparams: reqparams
      }),
      url: serviceBase + 'ForgotPassword',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function (results) {
      if (results.status_code === '1') {
        $('#myModal').modal('hide')
      }
      $('.rest-button').removeClass('disabled')
      $('.rest-button').text('Reset Password')
      $('.rest-password-textfield').val('')
      Data.toast(results)
    })
  }

  // doForgot request
  $scope.doResetPassword = function (reqparams) {
    var query = $location.path()
    var data = query.split('/')
    var key = data[2]
    $http({
      method: 'post',
      data: $.param({
        reqparams: reqparams,
        key: key
      }),
      url: serviceBase + 'doResetPassword',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function (results) {
      Data.toast(results)
      $location.path('/')
    })
  }

  $scope.loadReset = function () {
    var query = $location.path()
    var data = query.split('/')
    var key = data[2]
    $http({
      method: 'post',
      data: $.param({
        key: key
      }),
      url: serviceBase + 'checkResetKey',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).success(function (results) {
      if (results.status_code === '0') {
        Data.toast(results)
        $location.path('/')
      }
    })
  }
})

app.run(function ($rootScope, $route, $location) {
  // Data

  $rootScope.$on('$routeChangeStart', function (event, next, current) {
    if ($location.$$host == 'localhost') {
      var hostName = $location.$$protocol + '://' + $location.$$host
      $rootScope.baseUrl = hostName + '/client/'
    } else {
      $rootScope.baseUrl = 'https://www.rejuvenair.co.uk/' // $location.$$protocol + '://' + $location.$$host + '/client/';
    }

    $rootScope.loading = true
    $rootScope.onlyNumbers = /^\d+$/
  })
})

app.directive('datepicker', function () {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, elem, attrs, ngModelCtrl) {
      var updateModel = function (dateText) {
        scope.$apply(function () {
          ngModelCtrl.$setViewValue(dateText)
        })
      }
      var options = {
        dateFormat: 'dd/mm/yy',
        onSelect: function (dateText) {
          updateModel(dateText)
        }
      }
      elem.datepicker(options)
    }
  }
})

/* text trim */
app.filter('cut', function () {
  return function (value, wordwise, max, tail) {
    if (!value) return ''

    max = parseInt(max, 10)
    if (!max) return value
    if (value.length <= max) return value

    value = value.substr(0, max)
    if (wordwise) {
      var lastspace = value.lastIndexOf(' ')
      if (lastspace !== -1) {
        //Also remove . and , so its gives a cleaner result.
        if (
          value.charAt(lastspace - 1) === '.' ||
          value.charAt(lastspace - 1) === ','
        ) {
          lastspace = lastspace - 1
        }
        value = value.substr(0, lastspace)
      }
    }

    return value + (tail || ' �')
  }
})
