// app.controller('clientCtrl', function ($scope, $timeout, $rootScope, $window, $route, $routeParams, $location, $http, Data, DOBService, Alertify) {
app.controller('clientCtrl', function (
  $scope,
  $timeout,
  $rootScope,
  $window,
  $route,
  $routeParams,
  $location,
  $http,
  $anchorScroll
) {
  // Data

  //  if (!$rootScope.carerLoggedIn) {
  // 		// $location.path('/logout');
  // 	}

  // $scope.apptStarted = $rootScope.apptStarted;
  $scope.login = {}
  $scope.signup = {}

  // $scope.mindate = new Date();
  var xmlHttp

  // function srvTime() {
  //     try {
  //         //FF, Opera, Safari, Chrome
  //         xmlHttp = new XMLHttpRequest();
  //     } catch (err1) {
  //         //IE
  //         try {
  //             xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
  //         } catch (err2) {
  //             try {
  //                 xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
  //             } catch (eerr3) {
  //                 //AJAX not supported, use CPU time.
  //                 alert("AJAX not supported");
  //             }
  //         }
  //     }
  //     xmlHttp.open('HEAD', window.location.href.toString(), false);
  //     xmlHttp.setRequestHeader("Content-Type", "text/html");
  //     xmlHttp.send('');
  //     return xmlHttp.getResponseHeader("Date");
  // }

  // var st = srvTime();
  // var serverDate = new Date(st);
  // $scope.dtmax = serverDate;

  // $scope.today = function () {
  //     $scope.dt = serverDate;
  // };
  // $scope.today();

  if ($location.$$host == 'localhost') {
    // var hostName = $location.$$protocol + '://' + $location.$$host;
    // $rootScope.baseUrl = hostName + '/client/';

    $rootScope.baseUrl = '/'
  } else {
    // $rootScope.baseUrl = 'https://www.rejuvenair.co.uk/'; // $location.$$protocol + '://' + $location.$$host + '/client/';
    $rootScope.baseUrl = 'https://rejuvenair.app/'
  }

  // var date = serverDate;
  // $scope.minDate = date.setDate((new Date()).getDate());

  $scope.loadCurrntOrgPageTitle = function (orgPageTitle) {
    $rootScope.orgPageTitle = orgPageTitle
  }

  $scope.toggleSideBar = function () {
    $('#sidebar').css('left', '0!important')
    console.log('hide')
  }

  $scope.loadMainMenuCurrntActiveClass = function (ActiveClass) {
    $rootScope.activeClass = ActiveClass
  }

  var serviceBase = 'api/v1/admin/RestAPI.php'
  var clientServiceBase = 'api/v1/admin/ClientRestAPI.php'
  $scope.BaseUrl = 'uploads/'

  $scope.getCurrentDayWithFormat = function (dateObj) {
    var monthNames = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec'
    ]
    var dayNames = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday'
    ]
    var todayMonth = monthNames[dateObj.getMonth()]
    var todayDate = dateObj.getDate()
    var todayYear = dateObj.getUTCFullYear()
    var todayDay = dayNames[dateObj.getDay()]
    return todayDay + ', ' + todayDate + ' ' + todayMonth + ' ' + todayYear
  }

  /* FUNCTIONS START */

  // Profile Photo
  // $scope.fileReaderSupported = window.FileReader != null;
  // $scope.profilePhotoChanged = function (files) {
  //     if (files != null) {
  //         var file = files[0];
  //         if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
  //             $timeout(function () {
  //                 var fileReader = new FileReader();
  //                 fileReader.readAsDataURL(file);
  //                 fileReader.onload = function (e) {
  //                     $timeout(function () {
  //                         $scope.signup.ProfilePhoto = e.target.result;
  //                     });
  //                 }
  //             });

  //         }
  //     }
  // };

  // $scope.profilePhotoChangedCustomer = function (files) {
  //     var maxwidth = 400;
  //     var maxheight = 400;
  //     var imgwidth = 0;
  //     var imgheight = 0;
  //     if (files != null) {
  //         var file = files[0];
  //         if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
  //             $timeout(function () {
  //                 var fileReader = new FileReader();
  //                 fileReader.readAsDataURL(file);
  //                 fileReader.onload = function (e) {
  //                     $timeout(function () {
  //                         //$scope.signup.ProfilePhoto = e.target.result;
  //                     });
  //                 }
  //                 fileReader.onload = (function (theFile) {
  //                     var image = new Image();
  //                     image.src = theFile.target.result;

  //                     image.onload = function () {
  //                         // access image size here
  //                         var imgwidth = this.width;
  //                         var imgheight = this.height;
  //                         if (imgwidth <= maxwidth && imgheight <= maxheight) {
  //                             $('#img-upload').attr('src', this.src);
  //                             $('#img-msg').html('');
  //                             $scope.signup.ProfilePhoto = this.src;
  //                         } else {
  //                             $('#img-msg').html('Please upload image of 400px X 400px');
  //                         }
  //                     };
  //                 });
  //             });

  //         }
  //     }
  // };

  // $scope.attachmentFile = function (files) {
  //     if (files != null) {
  //         var file = files[0];
  //         var type = files[0].type;
  //         if ((type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') || (type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') || (type == 'application/vnd.ms-excel') || (type == 'application/msword') || (type == 'image/jpeg') || (type == 'image/jpg') || (type == 'image/png') || (type == 'application/pdf')) {
  //             $timeout(function () {
  //                 var fileReader = new FileReader();
  //                 fileReader.readAsDataURL(file);
  //                 fileReader.onload = function (e) {
  //                     $timeout(function () {
  //                         $scope.clientUpload.FileUpload = e.target.result;
  //                         $scope.showAttachMsg = true;
  //                     });
  //                 }
  //             });
  //         } else {
  //             Alertify.alert('Invalid file type');
  //         }

  //     }
  // };

  /**Set Gender**/
  $scope.setGender = function (id) {
    var $radios = $('input:radio[name=Gender]')
    if (id == 'Mr') {
      $radios.filter('[value=Male]').prop('checked', true)
    } else if (id == 'Mrs') {
      $radios.filter('[value=Female]').prop('checked', true)
    } else if (id == 'Miss') {
      $radios.filter('[value=Female]').prop('checked', true)
    } else if (id == 'Dr') {
      $radios.filter('[value=Male]').prop('checked', true)
    } else if (id == 'Prof') {
      $radios.filter('[value=Male]').prop('checked', true)
    } else if (id == 'Other') {
      $radios.filter('[value=Male]').prop('checked', true)
    }
  }
  /**Set Gender**/

  /**Set Default Parametres**/
  $scope.signup.CustomerTitle = 'Mr'
  $scope.signup.Gender = 'Male'
  $scope.signup.support = 'Personal Care'
  $scope.signup.makeenq = 'Self'

  $scope.clientID = $routeParams.id

  // view client-load
  // $scope.getClientByID = function () {

  //     $(".clent-login-detail").css("height", "250px");
  //     $scope.loading = true;
  //     var CustomerID = $routeParams.id;
  //     var request = '{"ServiceRequestID":"6","CustomerID":"' + CustomerID + '"}';

  //     $("#nok").hide();
  //     $("#gp").hide();
  //     $("#client").show();

  //     $scope.doClientDetails = function () {
  //         console.log('doClientHome');
  //         $("#nok").hide();
  //         $("#gp").hide();
  //         $("#client").show();
  //     };
  //     $scope.doClientNOK = function () {
  //         console.log('doClientNOK');
  //         $("#client").hide();
  //         $("#gp").hide();
  //         $("#nok").show();
  //     };
  //     $scope.doClientGP = function () {
  //         console.log('doClientGP');
  //         $("#client").hide();
  //         $("#nok").hide();
  //         $("#gp").show();
  //     };

  //     $.ajax({
  //         url: clientServiceBase,
  //         type: 'POST', // http method
  //         data: request, // data to submit
  //         dataType: "json",
  //         async: false,

  //         success: function (results, status, xhr) {
  //             //                 console.log('results.ResponseData2: ' + JSON.stringify (results.ResponseData2));

  //             $rootScope.fullName = results.ResponseData2.full_name;
  //             console.log('$rootScope.fullName: ' + $rootScope.fullName);
  //             $scope.fullName = $rootScope.fullName;

  //             if (results.StatusCode == 1) {
  //                 //$scope.loading = false;
  //                 $scope.signup = results.ResponseData2;

  //                 var DateT = results.DateT;
  //                 var StartTimeH = results.StartTimeH;
  //                 var StartTimeM = results.StartTimeM;

  //                 var d = new Date(results.DateT);
  //                 d.setHours(StartTimeH);
  //                 d.setMinutes(StartTimeM);

  //                 $scope.signup.StartTime = d;

  //                 $scope.allAllergiesList = results.Allergies;
  //                 $(".clent-login-detail").css("height", "auto");
  //                 $("#PersonalCare").prop("checked", results.PersonalCare);
  //                 $("#Medication").prop("checked", results.Medication);
  //                 $("#SelfSupport").prop("checked", results.SelfSupport);
  //                 $("#SupportedLiving").prop("checked", results.SupportedLiving);
  //                 $("#Outreach").prop("checked", results.Outreach);
  //                 $("#ComplexCare").prop("checked", results.ComplexCare);

  //                 $("#editClient").prop("href", base_url + "organisation/client/edit/" + CustomerID);
  //                 $("#ClientSum").prop("href", base_url + "organisation/client/summary/" + CustomerID);
  //                 $("#FileUp").prop("href", base_url + "organisation/client/fileuploads/" + CustomerID);
  //                 $("#GP").prop("href", base_url + "organisation/client/gpnotes/" + CustomerID);
  //                 $("#Out").prop("href", base_url + "organisation/client/outcomes/" + CustomerID);
  //                 $("#Task").prop("href", base_url + "organisation/client/tasks/" + CustomerID);
  //                 $("#Visit").prop("href", base_url + "organisation/client/visits/" + CustomerID);
  //                 $("#CP").prop("href", base_url + "organisation/client/careplan/" + CustomerID);
  //                 $("#AS").prop("href", base_url + "organisation/client/assessments/" + CustomerID);
  //                 $("#CN").prop("href", base_url + "organisation/client/carenotes/" + CustomerID);

  //                 $("#fuadd").prop("href", base_url + "organisation/client/addfileuploads/" + CustomerID);
  //                 $("#gpadd").prop("href", base_url + "organisation/client/addGPNotes/" + CustomerID);
  //                 $("#outadd").prop("href", base_url + "organisation/client/outcome/create/" + CustomerID);
  //                 $("#taskadd").prop("href", base_url + "organisation/client/task/create/" + CustomerID);

  //                 $("#visitBasicDetail").prop("href", base_url + "organisation/client/visitbasicinfo/" + CustomerID);
  //                 $("#visitListing").prop("href", base_url + "organisation/client/visits/" + CustomerID);

  //                 if (results.StatusID == 1) {
  //                     $("#visitadd").prop("href", base_url + "organisation/client/visit/create/" + CustomerID);
  //                 } else {
  //                     $("#visitadd").css("pointer-events", "none");
  //                 }

  //                 $timeout(function () {
  //                     $(".meter > span").fadeOut();
  //                     $scope.loading = false;
  //                 }, 1500);
  //             }

  //         },
  //         error: function (jqXhr, textStatus, errorMessage) {
  //             console.log('errorMessage' + ' - ' + errorMessage);
  //             $scope.loading = false;
  //         }
  //     });
  // };

  $scope.setDefaultDate = function () {
    $scope.signup.DateOfBirth = '01-01-1950'
  }

  $scope.formatDate = function (date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear()

    if (month.length < 2) month = '0' + month
    if (day.length < 2) day = '0' + day

    return [year, month, day].join('-')
  }

  $scope.formatDate2 = function (date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear()

    if (month.length < 2) month = '0' + month
    if (day.length < 2) day = '0' + day

    return [year, month, day].join(',')
  }

  // $scope.nowDate = $scope.formatDate(serverDate);

  console.log('$location: ' + $location.path())

  // logout request
  $scope.logout = function () {
    $scope.loading = true
    Data.get('Logout').then(function (results) {
      //Data.toast(results);
      $route.reload()
      $scope.loading = false
    })
  }

  // Edit qualification
  // $scope.editQualificationload = function () {
  //     var QualificationID = $routeParams.id;
  //     $scope.loading = true;
  //     $http({
  //             method: 'post',
  //             data: $.param({
  //                 QualificationID: QualificationID
  //             }),
  //             url: serviceBase + 'getUpdateQualificationID',
  //             headers: {
  //                 'Content-Type': 'application/x-www-form-urlencoded'
  //             }
  //         })
  //         .success(function (results) {
  //             if (results.status_code == "1") {
  //                 $rootScope.resultData = results.response_data;
  //                 $scope.loading = false;
  //             }
  //         });
  // };

  $scope.toggleShow = function () {
    if ($scope.showStartEvent == true) {
      $scope.signup.statusid = '1'
    } else {
      $scope.signup.statusid = '2'
    }
    $scope.showStartEvent = !$scope.showStartEvent
  }

  $scope.IsVisible = false

  $scope.changeStatus = function (id) {
    if (id == 1) {
      $scope.deactivebtn = $scope.deactivebtn
      $scope.activebtn = !$scope.activebtn
    } else if (id == 2) {
      $scope.deactivebtn = $scope.deactivebtn
      $scope.activebtn = !$scope.activebtn
    }
  }

  $scope.ShowHide = function () {
    $scope.IsVisible = $scope.IsVisible ? false : true
  }
})

app.run(function ($rootScope, $route, $location, Data) {
  $rootScope.$on('$routeChangeStart', function (event, next, current) {
    if ($location.$$host == 'localhost') {
      // var hostName = $location.$$protocol + '://' + $location.$$host;
      // $rootScope.baseUrl = hostName + '/client/';

      $rootScope.baseUrl = '/'
    } else {
      // $rootScope.baseUrl = 'https://www.rejuvenair.co.uk/'; // $location.$$protocol + '://' + $location.$$host + '/client/';
      $rootScope.baseUrl = 'https://rejuvenair.app/'
    }

    $rootScope.loading = true
    $rootScope.loading = false
  })
})

/* trim text */

// app.filter('cut', function () {
//     return function (value, wordwise, max, tail) {
//         if (!value) return '';

//         max = parseInt(max, 10);
//         if (!max) return value;
//         if (value.length <= max) return value;

//         value = value.substr(0, max);
//         if (wordwise) {
//             var lastspace = value.lastIndexOf(' ');
//             if (lastspace !== -1) {
//                 //Also remove . and , so its gives a cleaner result.
//                 if (value.charAt(lastspace - 1) === '.' || value.charAt(lastspace - 1) === ',') {
//                     lastspace = lastspace - 1;
//                 }
//                 value = value.substr(0, lastspace);
//             }
//         }
//         return value + (tail || ' �');
//     };
// });
