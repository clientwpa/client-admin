app.controller('historyCtrl', function (
  $filter,
  $scope,
  $timeout,
  $rootScope,
  $window,
  $route,
  $routeParams,
  $location,
  $http,
  toaster
) {
  // Data

  $rootScope.currentLocation = ''
  $scope.currentLocation = ''
  $scope.location = $rootScope.location

  //  var xmlHttp;
  //
  //     function srvTime() {
  //         try {
  //             //FF, Opera, Safari, Chrome
  //             xmlHttp = new XMLHttpRequest();
  //         }
  //         catch (err1) {
  //             //IE
  //             try {
  //                 xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
  //             }
  //             catch (err2) {
  //                 try {
  //                     xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
  //                 }
  //                 catch (eerr3) {
  //                     //AJAX not supported, use CPU time.
  //                     alert("AJAX not supported");
  //                 }
  //             }
  //         }
  //         xmlHttp.open('HEAD', window.location.href.toString(), false);
  //         xmlHttp.setRequestHeader("Content-Type", "text/html");
  //         xmlHttp.send('');
  //         return xmlHttp.getResponseHeader("Date");
  //     }
  //
  //     var st = srvTime();
  //     var serverDate = new Date(st);
  //     $scope.dtmax = serverDate;
  //
  //     $scope.today = function () {
  //         $scope.dt = serverDate;
  //     };
  //     $scope.today();

  // if ($location.$$host == 'localhost') {
  //   var hostName = $location.$$protocol + '://' + $location.$$host;
  //   var base_url = hostName + '/ourwebsite/';
  // } else {
  //   var base_url = $location.$$protocol + '://' + $location.$$host + '/newsite/';
  // }

  $scope.loadCurrntOrgPageTitle = function (orgPageTitle) {
    $rootScope.orgPageTitle = orgPageTitle
  }

  $scope.loadMainMenuCurrntActiveClass = function (ActiveClass) {
    $scope.loading = true
    $rootScope.activeClass = ActiveClass
    $scope.loading = false
  }

  /* FUNCTIONS */

  function showToast (type, title, value) {
    toaster.pop(type, title, '<b>' + value + '</b>', 5000, 'trustedHtml')
  }

  var serviceBase = 'api/restApi.php?Request='
  var request =
    '{ "ServiceRequestID": "7", "unit_id": "' + $scope.location.unit_id + '"}'

  function isEmpty (obj) {
    return Object.keys(obj).length === 0
  }

  $scope.items = [
    {
      id: 0,
      unit_id: 0,
      location_id: 0,
      unit_name: 'No Favourites',
      unit_location: ''
    }
  ]

  $scope.loadCacheFavs = function () {
    var cachedFavs = localStorage.getItem('cachedFavs')

    if (cachedFavs != null) {
      if (isEmpty(cachedFavs) != true) {
        // load cache object to drop menu
        console.log('cachedFavs: ' + cachedFavs)
        $scope.items = JSON.parse(cachedFavs)
      } else {
        console.log('cachedFavs Empty ')
      }
    }
  }

  $scope.doChangeLocation = function () {
    let selectedUnit = $scope.selectedUnit
    if (
      selectedUnit.unit_name == 'No Favourites' ||
      selectedUnit.unit_name == $scope.location.unit_name
    ) {
      $scope.selectedUnit = $scope.location
      return
    }

    var serviceBase = 'api/restApi.php?Request='
    var request =
      '{ "ServiceRequestID": "1", "location": "' +
      selectedUnit.location_id +
      '"}'

    $.ajax({
      url: serviceBase,
      type: 'POST',
      data: request,
      async: false,

      success: function (data, status, xhr) {
        data = JSON.parse(data)

        if (data.ResponseData.Data[0]) {
          $scope.hasData = true

          $scope.location = data.ResponseData.Data[0]
          $rootScope.location = $scope.location

          console.log('$scope.location: ' + JSON.stringify($scope.location))
          $scope.location.previousLocation = $scope.location.location_id
          $scope.location.location_id = parseInt($scope.location.location_id)

          showToast('info', 'Location Changed', 'Sucessfully')

          $route.reload()
        } else {
          $scope.hasData = false
          if ((data.ResponseData.Data[0] = 'undefined')) {
            // selectedUnit.unit_name
          } else {
            Swal.fire('Location Not Found')
          }

          $scope.selectedUnit = $scope.location
          console.log('Not Found')
        }

        console.log(
          'selected location profile: ' + JSON.stringify($scope.location)
        )
      },
      error: function (jqXhr, textStatus, errorMessage) {
        console.log(
          'errorMessage: ' +
            JSON.stringify(errorMessage) +
            ' : ' +
            JSON.stringify(jqXhr + ' : ' + JSON.stringify(textStatus))
        )
        return errorMessage
      }
    })
  }

  $scope.selectedUnit = $scope.location
  $scope.loadCacheFavs()

  $scope.selectedUnitHistory = function () {
    console.log('selectedUnit: ' + JSON.stringify($scope.selectedUnit))
  }

  $scope.loadHistory = function () {
    $.ajax({
      url: serviceBase,
      type: 'POST',
      data: request,
      async: false,

      success: function (data, status, xhr) {
        data = JSON.parse(data)

        if (data.ResponseData.Data[0]) {
          $scope.hasData = true

          $scope.nextReadings = data.ResponseData.Data

          console.log('hasData-records: ' + $scope.nextReadings.length)
          $scope.firstReading = $scope.nextReadings[0]

          if (data.ResponseData.Data[1]) {
            $scope.secondReading = $scope.nextReadings[1]
          } else {
            $scope.secondReading = $scope.nextReadings[0]
          }

          if (data.ResponseData.Data[2]) {
            $scope.thirdReading = $scope.nextReadings[2]
          } else {
            $scope.thirdReading = $scope.nextReadings[0]
          }

          console.log('newestReading: ' + JSON.stringify($scope.firstReading))

          /* 
            $scope.firstReading
            {
              "id": "150",
              "unit_id": "1",
              "Date_and_Time": "2021-12-09 11:37:51",
              "Temperature": "20.4",
              "Relative_Humidity": "52.5",
              "PM25": "2",
              "TVOC": "370",
              "CO2": "866",
              "CO": "\u0000",
              "Air_Pressure": "1015.1",
              "Ozone": "10.2",
              "NO2": "16.7",
              "Virus_Index": "2"
            }
            */

          let CO2 = $scope.firstReading.CO2
          let temperature = $scope.firstReading.Temperature
          let relativeHumidity = $scope.firstReading.Relative_Humidity

          $scope.co2CurrentStatus = 'green'
          $scope.tempCurrentStatus = 'green'
          $scope.HumidityCurrentStatus = 'green'

          /* maybe case is a better method */
          if (CO2 >= 1000 && CO2 < 2500) {
            $scope.co2CurrentStatus = 'orange'
          } else if (CO2 >= 2500) {
            $scope.co2CurrentStatus = 'red'
          } else if (temperature >= 27 && temperature < 30) {
            $scope.tempCurrentStatus = 'orange'
          } else if (temperature > 30) {
            $scope.tempCurrentStatus = 'red'
          } else if (relativeHumidity >= 50 && relativeHumidity < 65) {
            $scope.HumidityCurrentStatus = 'orange'
          } else if (relativeHumidity >= 65) {
            $scope.HumidityCurrentStatus = 'red'
          }

          Highcharts.chart('chart', {
            data: {
              table: 'datatable'
            },
            chart: {
              type: 'line',
              marginTop: 9,
              widths: 500
            },
            title: {
              text: '',
              style: {
                fontWeight: 'bold'
              }
            },
            credits: {
              href: '', // history or something else
              // href: 'https://rejuvenair.co.uk/',
              text:
                'Location Id: ' +
                $scope.location.location_id +
                ' - Unit Id: ' +
                $scope.location.unit_id //Source: Rejuvenair"
            },
            yAxis: {
              allowDecimals: false,
              title: {
                text: 'Values',
                x: 0 // offset
              }
            },
            tooltip: {
              useHTML: true,
              headerFormat:
                '<span class="tooltipHeader">{series.name} ... </span>',
              pointFormat:
                '<br/> <div class="tooltipPointWrapper">+++' +
                '<span class="tooltipPoint">{point.y}^^^</span>' +
                '<span class="tooltipValueSuffix"> : @</span></div>' +
                '<span class="tooltipLine"></span> <br/>' +
                '<span style="color:{point.color}">Reading: </span> {point.name}',
              style: {
                color: '#fff'
              },
              valueDecimals: 0,
              backgroundColor: '#000',
              borderColor: '#000',
              borderRadius: 10,
              borderWidth: 3
            },

            xAxis: {
              categories: [
                '1',
                '2',
                '3'
              ] /* Moves xAxis to Center below not working for mobile view */,
              labels: {
                align: 'left',
                x: -5,
                y: 15
              }
            },

            legend: {
              layout: 'vertical',
              floating: true,
              align: 'center',
              x: 25,
              verticalAlign: 'top',
              y: -9
            },
            series: [
              {
                index: 0,
                color: $scope.HumidityCurrentStatus,
                name: 'Humidity',
                data: [
                  /*  we could build this object for series(s) as an arrange of objects on success api response above */

                  {
                    y: parseInt($scope.firstReading.Relative_Humidity),
                    option: 1,
                    name: 'First'
                  },
                  {
                    y: parseInt($scope.secondReading.Relative_Humidity),
                    option: 2,
                    name: 'Second'
                  },
                  {
                    y: parseInt($scope.thirdReading.Relative_Humidity),
                    option: 3,
                    name: 'Third'
                  }
                ],

                zones: [
                  {
                    value: 50,
                    color: 'green' //color: $scope.HumidityCurrentStatus
                  },
                  {
                    value: 65,
                    color: 'orange' //color: $scope.HumidityCurrentStatus
                  },
                  {
                    value: 200,
                    color: 'red' //color: $scope.HumidityCurrentStatus
                  }
                ]
              },
              {
                index: 1,
                color: $scope.tempCurrentStatus,
                name: 'Temperature',
                data: [
                  {
                    y: parseInt($scope.firstReading.Temperature),
                    option: 1,
                    name: 'First'
                  },
                  {
                    y: parseInt($scope.secondReading.Temperature),
                    option: 2,
                    name: 'Second'
                  },
                  {
                    y: parseInt($scope.thirdReading.Temperature),
                    option: 3,
                    name: 'Third'
                  }
                ],

                zones: [
                  {
                    value: 0,
                    color: 'orange' //$scope.tempCurrentStatus
                  },
                  {
                    value: 27,
                    color: 'green' //$scope.tempCurrentStatus
                  },
                  {
                    value: 30,
                    color: 'orange' //$scope.tempCurrentStatus
                  },
                  {
                    value: 50,
                    color: 'red' //$scope.tempCurrentStatus
                  }
                ]
              },
              {
                index: 2,
                color: $scope.co2CurrentStatus,
                name: 'CO2',
                data: [
                  {
                    y: parseInt($scope.firstReading.CO2),
                    option: 1,
                    name: 'First'
                  },
                  {
                    y: parseInt($scope.secondReading.CO2),
                    option: 2,
                    name: 'Second'
                  },
                  {
                    y: parseInt($scope.thirdReading.CO2),
                    option: 3,
                    name: 'Third'
                  }
                ],

                zones: [
                  // {
                  //   value: 1000, // - to 1000
                  //   color: "green" //: $scope.co2CurrentStatus
                  // }, {
                  //   value: 2500, // from 1001 to 2500
                  //   color: "orange" //: $scope.co2CurrentStatus
                  // },
                  // {
                  //   value: 5000, // from 2501 to 5000
                  //   color: "red" //: $scope.co2CurrentStatus
                  // },

                  {
                    value: 840, // - should be as above ... // Simulation Demo Only
                    color: 'green' //: $scope.co2CurrentStatus
                  },
                  {
                    value: 850, // from 1001 to 2500
                    color: 'orange' //: $scope.co2CurrentStatus
                  },
                  {
                    value: 1000, // from 2501 to 5000
                    color: 'red' //: $scope.co2CurrentStatus
                  }
                ]
              }
            ]
          })

          $scope.$apply()
        } else {
          $scope.hasData = false
          lastReading = []
          $scope.firstReading = []
          $scope.console.log('nextReading Not Found')
        }
      },
      error: function (jqXhr, textStatus, errorMessage) {
        console.log(
          'errorMessage: ' +
            JSON.stringify(errorMessage) +
            ' : ' +
            JSON.stringify(jqXhr + ' : ' + JSON.stringify(textStatus))
        )
        $scope.hasData = false
        return errorMessage
      }
    })

    $rootScope.loadHistory = setTimeout(function () {
      $scope.loadHistory()
    }, 60000)
  }

  $scope.doLocationLoad = function () {}

  setTimeout(function () {
    $scope.loadHistory()
  }, 1000)
})

app.run(function ($rootScope, $route, $location) {
  $rootScope.$on('$routeChangeStart', function (event, next, current) {
    if ($location.$$host == 'localhost') {
      var hostName = $location.$$protocol + '://' + $location.$$host
      $rootScope.baseUrl = hostName + '/client/'
    } else {
      $rootScope.baseUrl = 'https://www.rejuvenair.co.uk/' // $location.$$protocol + '://' + $location.$$host + '/client/';
    }

    $rootScope.loading = true
    $rootScope.onlyNumbers = /^\d+$/
  })
})

app.directive('datepicker', function () {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, elem, attrs, ngModelCtrl) {
      var updateModel = function (dateText) {
        scope.$apply(function () {
          ngModelCtrl.$setViewValue(dateText)
        })
      }
      var options = {
        dateFormat: 'dd/mm/yy',
        onSelect: function (dateText) {
          updateModel(dateText)
        }
      }
      elem.datepicker(options)
    }
  }
})

/* text trim */
app.filter('cut', function () {
  return function (value, wordwise, max, tail) {
    if (!value) return ''

    max = parseInt(max, 10)
    if (!max) return value
    if (value.length <= max) return value

    value = value.substr(0, max)
    if (wordwise) {
      var lastspace = value.lastIndexOf(' ')
      if (lastspace !== -1) {
        //Also remove . and , so its gives a cleaner result.
        if (
          value.charAt(lastspace - 1) === '.' ||
          value.charAt(lastspace - 1) === ','
        ) {
          lastspace = lastspace - 1
        }
        value = value.substr(0, lastspace)
      }
    }

    return value + (tail || ' �')
  }
})
