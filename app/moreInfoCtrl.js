app.controller('moreInfoCtrl', function (
  $scope,
  $timeout,
  $rootScope,
  $window,
  $route,
  $routeParams,
  $location,
  $http,
  toaster
) {
  // Data

  $rootScope.currentLocation = ''
  $scope.currentLocation = ''

  //   var xmlHttp;
  //
  //     function srvTime() {
  //         try {
  //             //FF, Opera, Safari, Chrome
  //             xmlHttp = new XMLHttpRequest();
  //         }
  //         catch (err1) {
  //             //IE
  //             try {
  //                 xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
  //             }
  //             catch (err2) {
  //                 try {
  //                     xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
  //                 }
  //                 catch (eerr3) {
  //                     //AJAX not supported, use CPU time.
  //                     alert("AJAX not supported");
  //                 }
  //             }
  //         }
  //         xmlHttp.open('HEAD', window.location.href.toString(), false);
  //         xmlHttp.setRequestHeader("Content-Type", "text/html");
  //         xmlHttp.send('');
  //         return xmlHttp.getResponseHeader("Date");
  //     }
  //
  //     var st = srvTime();
  //     var serverDate = new Date(st);
  //     $scope.dtmax = serverDate;
  //
  //     $scope.today = function () {
  //         $scope.dt = serverDate;
  //     };
  //     $scope.today();

  // if ($location.$$host == 'localhost') {
  //   var hostName = $location.$$protocol + '://' + $location.$$host;
  //   var base_url = hostName + '/ourwebsite/';
  // } else {
  //   var base_url = $location.$$protocol + '://' + $location.$$host + '/newsite/';
  // }

  $scope.loadCurrntOrgPageTitle = function (orgPageTitle) {
    $rootScope.orgPageTitle = orgPageTitle
  }

  $scope.loadMainMenuCurrntActiveClass = function (ActiveClass) {
    $scope.loading = true
    $rootScope.activeClass = ActiveClass
    $scope.loading = false
  }

  /* FUNCTIONS */

  $scope.contactDetails = {
    name: '',
    email: '',
    number: '',
    site: '',
    message: ''
  }

  $scope.loginDetails = {
    studentnumber: ''
  }

  function showToast (type, title, value) {
    toaster.pop(type, title, '<b>' + value + '</b>', 5000, 'trustedHtml')
  }

  $scope.doInsertRequestDetails = function (reqparams) {
    console.log('reqparams' + JSON.stringify(reqparams))
    /* check how post is done from view login as example */

    if (reqparams.name == '' || reqparams.email == '' || reqparams.message) {
      showToast(
        'info',
        'Please Enter Some Details',
        'Minimum Required: (Name/Email/Message) Thank You'
      )
      return
    }

    var serviceBase = 'api/api.php?request='

    $http({
      method: 'post',
      data: $.param({
        reqparams: reqparams
      }),
      url: serviceBase + 'insertRequestDetails',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
      .success(function (results) {
        if (results.status == 'error') {
          console.log('insertRequestDetails error: ' + JSON.stringify(results))
        } else {
          //console.log('insertRequestDetails results: ' + JSON.stringify(results));
        }
      })
      .error(function (results) {
        console.log('insertRequestDetails error, see console ...')
      })

    $http({
      method: 'post',
      data: $.param({
        reqparams: reqparams
      }),
      url: serviceBase + 'sendMailEnquiry',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
      .success(function (results) {
        if (results.status == 'error') {
          console.log('sendMailEnquiry error: ' + JSON.stringify(results))
          showToast('info', 'Issue Sending Message!', 'Please Try Again Later')
        } else {
          console.log('sendMailEnquiry results: ' + JSON.stringify(results))
          showToast(
            'info',
            'Message Sent Successfully!',
            'We Will Be In Contact Soon :)'
          )
          // $scope.reloadRoute = function() { $route.reload(); }
          $route.reload()
        }
      })
      .error(function (results) {
        console.log('sendMailEnquiry error, see console ...')
        showToast('info', 'Issue Sending Message!', 'Please Try Again Later')
      })
  }

  $scope.scrollText =
    'Welcome to Himgiri Global Institute of Nature & Mankind ... Science of Piece Courses ... To Enrol or find out more, use the Contact_Us Page ... Practical Applied Learning :)'

  $('#marquee-left').text($scope.scrollText)
})

app.directive('marqueeLeft', [
  function () {
    return {
      link: function (scope, element, attrs, ctrl) {
        var marqueeLeft = $(element).liMarquee({
          circular: false,
          width: 'auto',
          height: 'auto',
          hoverStop: true
        })

        // var num = 0
        // element.bind('click', function () {
        //   num++
        //   if (num % 2 == 0) {
        //     marqueeLeft.liMarquee('play');
        //   } else {
        //     marqueeLeft.liMarquee('pause');
        //   }
        // });
      },
      restrict: 'A'
    }
  }
])

app.run(function ($rootScope, $route, $location) {
  // Data
  $rootScope.$on('$routeChangeStart', function (event, next, current) {
    $rootScope.baseUrl = 'https://www.rejuvenair.co.uk/'
  })
})

app.directive('datepicker', function () {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, elem, attrs, ngModelCtrl) {
      var updateModel = function (dateText) {
        scope.$apply(function () {
          ngModelCtrl.$setViewValue(dateText)
        })
      }
      var options = {
        dateFormat: 'dd/mm/yy',
        onSelect: function (dateText) {
          updateModel(dateText)
        }
      }
      elem.datepicker(options)
    }
  }
})

//text trim
app.filter('cut', function () {
  return function (value, wordwise, max, tail) {
    if (!value) return ''

    max = parseInt(max, 10)
    if (!max) return value
    if (value.length <= max) return value

    value = value.substr(0, max)
    if (wordwise) {
      var lastspace = value.lastIndexOf(' ')
      if (lastspace !== -1) {
        //Also remove . and , so its gives a cleaner result.
        if (
          value.charAt(lastspace - 1) === '.' ||
          value.charAt(lastspace - 1) === ','
        ) {
          lastspace = lastspace - 1
        }
        value = value.substr(0, lastspace)
      }
    }

    return value + (tail || ' �')
  }
})
