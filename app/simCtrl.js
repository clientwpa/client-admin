app.controller('simCtrl', function (
  $filter,
  $scope,
  $timeout,
  $rootScope,
  $window,
  $route,
  $routeParams,
  $location,
  $http
) {
  // Data

  $rootScope.currentLocation = ''
  $scope.currentLocation = ''
  $scope.dateTimeDBLastUpdated = '0000-00-00 00:00:00'
  $scope.lastReadingObjString = {}

  //  var xmlHttp;
  //
  //     function srvTime() {
  //         try {
  //             //FF, Opera, Safari, Chrome
  //             xmlHttp = new XMLHttpRequest();
  //         }
  //         catch (err1) {
  //             //IE
  //             try {
  //                 xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
  //             }
  //             catch (err2) {
  //                 try {
  //                     xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
  //                 }
  //                 catch (eerr3) {
  //                     //AJAX not supported, use CPU time.
  //                     alert("AJAX not supported");
  //                 }
  //             }
  //         }
  //         xmlHttp.open('HEAD', window.location.href.toString(), false);
  //         xmlHttp.setRequestHeader("Content-Type", "text/html");
  //         xmlHttp.send('');
  //         return xmlHttp.getResponseHeader("Date");
  //     }
  //
  //     var st = srvTime();
  //     var serverDate = new Date(st);
  //     $scope.dtmax = serverDate;
  //
  //     $scope.today = function () {
  //         $scope.dt = serverDate;
  //     };
  //     $scope.today();

  // if ($location.$$host == 'localhost') {
  //   var hostName = $location.$$protocol + '://' + $location.$$host;
  //   var base_url = hostName + '/ourwebsite/';
  // } else {
  //   var base_url = $location.$$protocol + '://' + $location.$$host + '/newsite/';
  // }

  $scope.loadCurrntOrgPageTitle = function (orgPageTitle) {
    $rootScope.orgPageTitle = orgPageTitle
  }

  $scope.loadMainMenuCurrntActiveClass = function (ActiveClass) {
    $scope.loading = true
    $rootScope.activeClass = ActiveClass
    $scope.activeClass = ActiveClass
    $scope.loading = false
  }

  /* FUNCTIONS */

  $scope.availToggle = function (status) {
    if ($('#availToggle').prop('checked') == true) {
      console.log('SIM: ON')
      $scope.doUpdateHistory()
    } else if ($('#availToggle').prop('checked') == false) {
      console.log('SIM: OFF')
      clearTimeout($rootScope.doUpdateHistorySim)
    }
    // $scope.updateAvailableStatus($scope.available === true ? '1' : '0');
  }

  var unitSimulation = document.getElementById('unitSimulation')

  $scope.doTimeUpdate = function () {
    /* Get Next Oldest Reading - Simulation Purpose 
    
    
    OR 

    Just Update where oldest date-time with current date-time

    */

    // $scope.list = [
    //   {

    //     "DateTime": "01/05/2021 00:00:00",
    //     "Temperature": '00.0',
    //     "RelativeHumidity": '00.0',
    //     "PM": 0,
    //     "TVOC": 0,
    //     "CO2": 0,
    //     "CO": 0,
    //     "AirPressure": '00.0',
    //     "Ozone": '00.0',
    //     "NO2": '00.0',
    //     "VirusIndex": 0
    //   }
    // ];

    /* --- */

    let newDateTime = new Date()

    $scope.newDate = $filter('date')(newDateTime, 'dd/MM/yyyy HH:mm')
    // $scope.$apply();
    /* Update Database with new date-time */

    /* --- */

    /* Read/Display new date-time from Database  */

    /* --- */
  } /* doTimeUpdate */

  $scope.doUpdateHistory = function () {
    // $scope.list.push($scope.currentValues);
    // $rootScope.lastReadingValue = $scope.currentValues;

    if ($('#unitSimulation').hasClass('simCol1')) {
      // console.log('unitSimulation: hasClass simCol1');
      $('#unitSimulation')
        .removeClass('simCol1')
        .addClass('simCol2')

      // $('#unitSimulation').removeClass("simCol1").addClass('simCol2');

      // unitSimulation.classList.remove("simCol1");
      // unitSimulation.classList.add("simCol2");

      // $("#unitSimulation").css("color", "#5cbc66");
    } else {
      // console.log('unitSimulation: hasClass simCol2');
      $('#unitSimulation')
        .removeClass('simCol2')
        .addClass('simCol1')

      // $('#unitSimulation').removeClass("simCol2").addClass('simCol1');

      // unitSimulation.classList.remove("simCol2");
      // unitSimulation.classList.add("simCol1");

      // $("#unitSimulation").css("color", "blue");
    }

    var serviceBase = 'api/restApi.php?Request='
    var request =
      '{ "ServiceRequestID": "3", "unit_id": "' +
      $rootScope.location.unit_id +
      '"}'

    $.ajax({
      url: serviceBase,
      type: 'POST',
      data: request,
      async: false,

      success: function (data, status, xhr) {
        data = JSON.parse(data)

        if (data.ResponseData.Data[0]) {
          $scope.hasData = true

          $scope.nextReading = data.ResponseData.Data[0]

          // console.log('nextReading: ' + JSON.stringify($scope.nextReading));

          // return;

          // update unit reading with new date
          var request =
            '{ "ServiceRequestID": "4", "id": "' + $scope.nextReading.id + '"}'

          $.ajax({
            url: serviceBase,
            type: 'POST',
            data: request,
            async: false,

            success: function (data, status, xhr) {
              data = JSON.parse(data)

              if (data.ResponseData) {
                $scope.hasData = true
                $scope.dateTimeDBLastUpdated = data.ResponseData

                $scope.nextReading.Date_and_Time = $scope.dateTimeDBLastUpdated
                $scope.lastReadingObjString = JSON.stringify($scope.nextReading)
                
                console.log('Updated With Date: ' + data.ResponseData)
              } else {
                $scope.hasData = false

                console.log('nextReading update error')
              }

              setTimeout(function () {
                $scope.$apply();
              }, 1000)
              
            }
          })
        } else {
          $scope.hasData = false

          console.log('nextReading Not Found')
        }
      }
    })

    $scope.doTimeUpdate()

    $rootScope.doUpdateHistorySim = setTimeout(function () {
      $scope.doUpdateHistory()
    }, 10000)
  }
  /* $scope.doUpdateHistory Finish */

  // setTimeout(function () {
  //   $scope.doUpdateHistory();
  // }, 3000);
})

app.run(function ($rootScope, $route, $location) {
  // Data

  $rootScope.$on('$routeChangeStart', function (event, next, current) {
    if ($location.$$host == 'localhost') {
      var hostName = $location.$$protocol + '://' + $location.$$host
      $rootScope.baseUrl = hostName + '/'
    } else {
      $rootScope.baseUrl = 'https://www.rejuvenair.co.uk/' // $location.$$protocol + '://' + $location.$$host + '/client/';
    }

    $rootScope.loading = true
    $rootScope.onlyNumbers = /^\d+$/
  })
})

app.directive('datepicker', function () {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, elem, attrs, ngModelCtrl) {
      var updateModel = function (dateText) {
        scope.$apply(function () {
          ngModelCtrl.$setViewValue(dateText)
        })
      }
      var options = {
        dateFormat: 'dd/mm/yy',
        onSelect: function (dateText) {
          updateModel(dateText)
        }
      }
      elem.datepicker(options)
    }
  }
})

/* text trim */
app.filter('cut', function () {
  return function (value, wordwise, max, tail) {
    if (!value) return ''

    max = parseInt(max, 10)
    if (!max) return value
    if (value.length <= max) return value

    value = value.substr(0, max)
    if (wordwise) {
      var lastspace = value.lastIndexOf(' ')
      if (lastspace !== -1) {
        //Also remove . and , so its gives a cleaner result.
        if (
          value.charAt(lastspace - 1) === '.' ||
          value.charAt(lastspace - 1) === ','
        ) {
          lastspace = lastspace - 1
        }
        value = value.substr(0, lastspace)
      }
    }

    return value + (tail || ' �')
  }
})