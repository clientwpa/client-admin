'use strict'
var app = angular.module('myApp', ['ngRoute', 'toaster']) // , 'ui.bootstrap', 'ngAnimate', 'validator.rules'

app
  .config([
    '$locationProvider',
    '$routeProvider',
    function ($locationProvider, $routeProvider, $anchorScroll) {
      $locationProvider.html5Mode(true)
      $routeProvider
        .when('/', {
          title: 'Home Page',
          templateUrl: 'home.html',
          controller: 'homeCtrl'
        })
        .when('/about', {
          title: 'About Page',
          templateUrl: 'about.html'
        })
        .when('/fav', {
          title: 'Favorites Page',
          templateUrl: 'fav.html',
          controller: 'favCtrl'
        })
        .when('/sim', {
          title: 'Unit Simulation Page',
          templateUrl: 'sim.html',
          controller: 'simCtrl'
        })
        .when('/history', {
          title: 'History Page',
          templateUrl: 'history.html',
          controller: 'historyCtrl'
        })
        .when('/login', {
          title: 'Login Page',
          templateUrl: 'login.php',
          controller: 'authCtrl'
        })
        .when('/contactus', {
          title: 'More Info Page',
          templateUrl: 'contactus.html',
          controller: 'moreInfoCtrl'
        })
        .when('/wip', {
          title: 'Work In Progress',
          templateUrl: 'home.html',
          controller: 'homeCtrl'
        })
        .when('/about', {
          title: 'About Page',
          templateUrl: 'about.html',
          controller: 'aboutCtrl'
        })
        .when('/gpslogin', {
          title: 'GPS Login',
          templateUrl: 'gps_login.html',
          controller: 'gpsCtrl'
        })
        .when('/gpsnearme', {
          title: 'GPS Near Me',
          templateUrl: 'gps_nearme.html',
          controller: 'gpsNearMeCtrl'
        })
        .when('/home', {
          title: 'Home Page',
          templateUrl: 'home.html',
          controller: 'homeCtrl'
        })
        .otherwise({
          title: 'Page Not Found',
          templateUrl: 'home.html',
          controller: 'homeCtrl'
        })
    }
  ])

  .run(function ($rootScope, $location) {
    $rootScope.$on('$routeChangeStart', function (event, next, current) {
      if ($location.$$host == 'localhost') {
        // var hostName = $location.$$protocol + '://' + $location.$$host;
        // $rootScope.baseUrl = hostName + '/';

        $rootScope.baseUrl = '/'
      } else {
        $rootScope.baseUrl = 'https://rejuvenair.app/'
      }

      $rootScope.loading = true
      $rootScope.onlyNumbers = /^\d+$/
    })
  })

app.run(function ($rootScope, $location, $anchorScroll, $routeParams) {
  $rootScope.$on('$routeChangeSuccess', function (newRoute, oldRoute) {
    clearTimeout($rootScope.doUpdateHistory)
    clearTimeout($rootScope.loadHistory)

    $location.hash($routeParams.scrollTo)
    console.log($location.path())
    $anchorScroll()
  })
})

function ngNicescroll ($rootScope) {
  var directive = {
    link: link
  }
  return directive

  function link (scope, element, attrs, controller) {
    var niceOption = scope.$eval(attrs.niceOption)

    var niceScroll = $(element).niceScroll(niceOption)
    niceScroll.onscrollend = function (data) {
      if (data.end.y >= this.page.maxh) {
        if (attrs.niceScrollEnd) scope.$evalAsync(attrs.niceScrollEnd)
      }
    }

    scope.$on('$destroy', function () {
      if (angular.isDefined(niceScroll)) {
        niceScroll.remove()
      }
    })
  }
}

app.directive('resize', function ($window) {
  return function (scope, element) {
    var w = angular.element($window)
    scope.getWindowDimensions = function () {
      return {
        h: w.height(),
        w: w.width()
      }
    }
    scope.$watch(
      scope.getWindowDimensions,
      function (newValue, oldValue) {
        scope.windowHeight = newValue.h
        scope.windowWidth = newValue.w

        scope.style = function () {
          return {
            height: newValue.h - 100 + 'px',
            width: newValue.w - 100 + 'px'
          }
        }
      },
      true
    )

    w.bind('resize', function () {
      scope.$apply()
    })
  }
})

app.directive('datepicker', function () {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, elem, attrs, ngModelCtrl) {
      var updateModel = function (dateText) {
        scope.$apply(function () {
          ngModelCtrl.$setViewValue(dateText)
        })
      }
      var options = {
        dateFormat: 'dd/mm/yy',
        onSelect: function (dateText) {
          updateModel(dateText)
        }
      }
      elem.datepicker(options)
    }
  }
})
