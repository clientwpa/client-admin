app.controller('favCtrl', function (
  $scope,
  $timeout,
  $rootScope,
  toaster,
  $window,
  $route,
  $routeParams,
  $location,
  $http
) {
  // Data

  $rootScope.currentLocation = ''
  $scope.currentLocation = ''
  $scope.isInFavList = true
  $scope.userId = '1'
  //  var xmlHttp;
  //
  //     function srvTime() {
  //         try {
  //             //FF, Opera, Safari, Chrome
  //             xmlHttp = new XMLHttpRequest();
  //         }
  //         catch (err1) {
  //             //IE
  //             try {
  //                 xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
  //             }
  //             catch (err2) {
  //                 try {
  //                     xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
  //                 }
  //                 catch (eerr3) {
  //                     //AJAX not supported, use CPU time.
  //                     alert("AJAX not supported");
  //                 }
  //             }
  //         }
  //         xmlHttp.open('HEAD', window.location.href.toString(), false);
  //         xmlHttp.setRequestHeader("Content-Type", "text/html");
  //         xmlHttp.send('');
  //         return xmlHttp.getResponseHeader("Date");
  //     }
  //
  //     var st = srvTime();
  //     var serverDate = new Date(st);
  //     $scope.dtmax = serverDate;
  //
  //     $scope.today = function () {
  //         $scope.dt = serverDate;
  //     };
  //     $scope.today();

  // if ($location.$$host == 'localhost') {
  //   var hostName = $location.$$protocol + '://' + $location.$$host;
  //   var base_url = hostName + '/ourwebsite/';
  // } else {
  //   var base_url = $location.$$protocol + '://' + $location.$$host + '/newsite/';
  // }

  $scope.loadCurrntOrgPageTitle = function (orgPageTitle) {
    $rootScope.orgPageTitle = orgPageTitle
  }

  $scope.loadMainMenuCurrntActiveClass = function (ActiveClass) {
    $scope.loading = true
    $rootScope.activeClass = ActiveClass
    $scope.activeClass = ActiveClass
    console.log('$scope.activeClass: ' + $scope.activeClass)
    $scope.loading = false
  }

  function showToast (type, title, value) {
    toaster.pop(type, title, '<b>' + value + '</b>', 5000, 'trustedHtml')
  }
  /* FUNCTIONS */

  var userId = $scope.userId
  $scope.favList = []
  $scope.colourChange = []
  $scope.favIsSelected = []
  $scope.fav = ''

  $scope.loadFavFromDB = function (userId) {
    var serviceBase = 'api/restApi.php?Request='
    var request = '{ "ServiceRequestID": "10", "user_id": "' + userId + '"}'
    // var request = '{ "ServiceRequestID": "10", "user_id": "1" }'; // example

    $.ajax({
      url: serviceBase,
      type: 'POST',
      data: request,
      async: false,

      success: function (data, status, xhr) {
        data = JSON.parse(data)
        console.log('data: ' + JSON.stringify(data))

        if (data.ResponseData) {
          localStorage.setItem('cachedFavs', JSON.stringify(data.ResponseData))
          $scope.hasData = true
          $scope.favList = data.ResponseData

          $scope.favList.forEach((fav, index) => {
            $scope.favIsSelected[index] = true
          })
        } else {
          $scope.hasData = false
        }
      },
      error: function (jqXhr, textStatus, errorMessage) {
        console.log(
          'errorMessage: ' +
            JSON.stringify(errorMessage) +
            ' : ' +
            JSON.stringify(jqXhr + ' : ' + JSON.stringify(textStatus))
        )
        return errorMessage
      }
    })

    return false
  }

  $scope.loadFavFromDB(userId)

  $scope.favChangeSelect = function (index) {
    $scope.fav = document.getElementById('favMainIndex' + index)

    if (!$scope.favIsSelected[index]) {
      $scope.colourChange[index] = 'red'

      $scope.fav.classList.remove('fa-heart-o')
      $scope.fav.classList.add('fa-heart')
      $scope.fav.style.color = 'red'

      $scope.favIsSelected[index] = true
      console.log('favChangeSelect')
      $scope.addFavtoDB(index)
      $scope.loadFavFromDB(userId)
    } else {
      $scope.colourChange[index] = 'white'

      $scope.fav.classList.remove('fa-heart')
      $scope.fav.classList.add('fa-heart-o')
      $scope.fav.style.color = 'blueviolet'

      $scope.favIsSelected[index] = false
      console.log('favChangeUnSelect')
      $scope.deleteFavfromDB(index)
      $scope.loadFavFromDB(userId)
    }
  }

  $scope.addFavtoDB = function (index) {
    var unitId = $scope.favList[index].unit_id
    var locationId = $scope.favList[index].location_id
    var unitName = $scope.favList[index].unit_name
    var unitLocation = $scope.favList[index].unit_location

    var message = ''

    var serviceBase = 'api/restApi.php?Request='

    var request =
      '{ "ServiceRequestID": "8", "unit_id": "' +
      unitId +
      '","user_id": "' +
      $scope.userId +
      '","location_id": "' +
      locationId +
      '","unit_name": "' +
      unitName +
      '","unit_location": "' +
      unitLocation +
      '" }'
    // var request = '{ "ServiceRequestID": "8", "unit_id": "4", "user_id": "4", "location_id": "1" }'; // example

    $.ajax({
      url: serviceBase,
      type: 'POST',
      data: request,
      async: false,

      success: function (data, status, xhr) {
        data = JSON.parse(data)
        console.log('data: ' + JSON.stringify(data))

        if (data.ResponseData && data.ResponseData == true) {
          $scope.hasData = true

          message = data.Message

          console.log('Fav Added: ' + message)

          showToast('success', 'Fav Added', 'Sucessfully')
        } else {
          $scope.hasData = false

          if (data.Message) {
            message = data.Message
          } else {
            message =
              'Error, please try again or notify your support contact if Error contines to happen. Thank You'
          }

          Swal.fire(message)
          console.log('Fav Not Added: ' + message)
        }
      },
      error: function (jqXhr, textStatus, errorMessage) {
        console.log(
          'errorMessage: ' +
            JSON.stringify(errorMessage) +
            ' : ' +
            JSON.stringify(jqXhr + ' : ' + JSON.stringify(textStatus))
        )
        return errorMessage
      }
    })

    return false
  }

  $scope.deleteFavfromDB = function (index) {
    var unitId = $scope.favList[index].unit_id
    var serviceBase = 'api/restApi.php?Request='
    var request =
      '{ "ServiceRequestID": "9", "unit_id": "' +
      unitId +
      '","user_id": "' +
      $scope.userId +
      '"}'
    console.log('request: ' + JSON.stringify(request))
    // var request = '{ "ServiceRequestID": "9", "unit_id": "4", "user_id": "4", "location_id": "4" }'; // example

    $.ajax({
      url: serviceBase,
      type: 'POST',
      data: request,
      async: false,

      success: function (data, status, xhr) {
        data = JSON.parse(data)
        console.log('data: ' + JSON.stringify(data))

        if (data.ResponseData && data.ResponseData == true) {
          $scope.hasData = true

          message = data.Message

          console.log('Fav Added: ' + message)

          showToast('success', 'Fav Removed', 'Sucessfully')
        } else {
          $scope.hasData = false

          if (data.Message) {
            message = data.Message
          } else {
            message =
              'Error, please try again or notify your support contact if Error contines to happen. Thank You'
          }

          Swal.fire(message)
          console.log('Fav Not Removed: ' + message)
        }
      },
      error: function (jqXhr, textStatus, errorMessage) {
        console.log(
          'errorMessage: ' +
            JSON.stringify(errorMessage) +
            ' : ' +
            JSON.stringify(jqXhr + ' : ' + JSON.stringify(textStatus))
        )
        return errorMessage
      }
    })
    return false
  }

  $scope.favChangeUnSelect = function (index) {}

  $scope.favToggle = function (status) {
    if ($('#availToggle').prop('checked') == true) {
      console.log('Fav: ON')
      $scope.doUpdateHistory()
    } else if ($('#availToggle').prop('checked') == false) {
      console.log('Fav: OFF')
      clearTimeout($rootScope.doUpdateHistorySim)
    }
  }
})

app.run(function ($rootScope, $route, $location) {
  // Data

  $rootScope.$on('$routeChangeStart', function (event, next, current) {
    if ($location.$$host == 'localhost') {
      var hostName = $location.$$protocol + '://' + $location.$$host
      $rootScope.baseUrl = hostName + '/client/'
    } else {
      $rootScope.baseUrl = 'https://www.rejuvenair.co.uk/' // $location.$$protocol + '://' + $location.$$host + '/client/';
    }

    $rootScope.loading = true
    $rootScope.onlyNumbers = /^\d+$/
  })
})

app.directive('datepicker', function () {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, elem, attrs, ngModelCtrl) {
      var updateModel = function (dateText) {
        scope.$apply(function () {
          ngModelCtrl.$setViewValue(dateText)
        })
      }
      var options = {
        dateFormat: 'dd/mm/yy',
        onSelect: function (dateText) {
          updateModel(dateText)
        }
      }
      elem.datepicker(options)
    }
  }
})

/* text trim */
app.filter('cut', function () {
  return function (value, wordwise, max, tail) {
    if (!value) return ''

    max = parseInt(max, 10)
    if (!max) return value
    if (value.length <= max) return value

    value = value.substr(0, max)
    if (wordwise) {
      var lastspace = value.lastIndexOf(' ')
      if (lastspace !== -1) {
        //Also remove . and , so its gives a cleaner result.
        if (
          value.charAt(lastspace - 1) === '.' ||
          value.charAt(lastspace - 1) === ','
        ) {
          lastspace = lastspace - 1
        }
        value = value.substr(0, lastspace)
      }
    }

    return value + (tail || ' �')
  }
})
