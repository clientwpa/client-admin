app.controller('sideMenuCtrl', function (
  $scope,
  $rootScope,
  $window,
  $route,
  $routeParams,
  $location,
  $http
) {
  $scope.orientationPortrait = true
  if ($rootScope.accessLevel != null) {
    $scope.accessLevel = $rootScope.accessLevel
  } else {
    /* Testing Only ... to save having to login with to see higher access level icons on side menu options */
    $scope.accessLevel = 5 // super admin
    $rootScope.accessLevel = 5
  }

  function srvTime () {
    try {
      xmlHttp = new XMLHttpRequest()
    } catch (err1) {
      //IE
      try {
        xmlHttp = new ActiveXObject('Msxml2.XMLHTTP')
      } catch (err2) {
        try {
          xmlHttp = new ActiveXObject('Microsoft.XMLHTTP')
        } catch (eerr3) {
          //AJAX not supported, use CPU time.
          alert('AJAX not supported')
        }
      }
    }
    xmlHttp.open('HEAD', window.location.href.toString(), false)
    xmlHttp.setRequestHeader('Content-Type', 'text/html')
    xmlHttp.send('')
    return xmlHttp.getResponseHeader('Date')
  }

  // $rootScope.baseUrl = 'https://www.rejuvenair.co.uk/';

  $scope.changeIcon = function (icon) {
    return
    setTimeout(function () {
      if (icon == 'mission') {
        $checkImgSrc = $('#mission').attr('src')
        console.log('$checkImgSrc: ' + $checkImgSrc)

        if ($checkImgSrc == 'img/icons/rocket4.gif') {
          document.getElementById('mission').src =
            'img/icons/rocketSelected1.gif'
          console.log('rocketSelected')
        } else {
          document.getElementById('mission').src = 'img/icons/rocket4.gif'
          console.log('rocketNormal')
        }
      }
    }, 750)
  }

  $scope.loadCurrntOrgPageTitle = function (orgPageTitle) {
    $rootScope.orgPageTitle = orgPageTitle
  }

  $scope.loadMainMenuCurrntActiveClass = function (ActiveClass) {
    $scope.loading = true
    $rootScope.activeClass = ActiveClass
    $scope.loading = false
  }

  $scope.doSideMenuPortrait = function () {
    console.log('Changed to portrait')
    $scope.orientationPortrait = true
    $rootScope.orientationPortrait = true
    $('.sideBarText').css('left', '-40.5vw')
    $('#bars')
      .find($('.fa'))
      .removeClass('fa-close')
      .addClass('fa-bars')
    $('#sidebar').animate(
      {
        left: '-44.75vw'
      },
      'fast'
    )
  }

  $scope.doSideMenuLandscape = function () {
    console.log('Changed to landscape')
    $scope.orientationPortrait = false
    $rootScope.orientationPortrait = false
    $('#bars')
      .find($('.fa'))
      .removeClass('fa-bars')
      .addClass('fa-close')
    $('.sideBarText').css('left', '5.5vw')
    $('#sidebar').animate(
      {
        left: '-44.75vw'
      },
      'fast'
    )
  }

  $scope.scrollToFindus = function (id) {
    console.log('id: ' + id)
    $location.path('/contactus')
    $location.hash(id)

    setTimeout(function () {
      $anchorScroll()
    }, 1000)
  }

  $scope.scrollTo = function (id) {
    console.log('id: ' + id)
    $location.path('/contactuslocations')
    $location.hash(id)

    setTimeout(function () {
      $anchorScroll()
    }, 1000)
  }
  var mql

  mql = window.matchMedia('(orientation: portrait)')
  if (mql.matches) {
    $scope.doSideMenuPortrait()
  } else {
    $scope.doSideMenuLandscape()
  }

  mql.addListener(function (m) {
    if (m.matches) {
      $scope.doSideMenuPortrait()
    } else {
      $scope.doSideMenuLandscape()
    }
  })

  if ($scope.orientationPortrait) {
    $('#worldmap svg').css('width', '200px')
    $('#worldmap svg').css('height', '200px')
  }

  $scope.doSideMenu = function () {
    var position = $('#sidebar').offset()

    if (position.left == 0) {
      $('#bars')
        .find($('.fa'))
        .removeClass('fa-close')
        .addClass('fa-bars')

      $('#sidebar').animate(
        {
          left: '-44.75vw'
        },
        'fast'
      )

      $scope.doSideMenuPortrait()
    } else {
      if ($scope.orientationPortrait) {
        $('#bars')
          .find($('.fa'))
          .removeClass('fa-bars')
          .addClass('fa-close')
        $('.sideBarText').css('left', '-40.5vw')

        $('#sidebar').animate(
          {
            left: '0'
          },
          'fast'
        )
      } else {
        if (
          $('#bars')
            .find($('.fa'))
            .hasClass('fa-bars')
        ) {
          $('#bars')
            .find($('.fa'))
            .removeClass('fa-bars')
            .addClass('fa-close')
          $('.sideBarText').css('left', '5.5vw')
        } else {
          $('#bars')
            .find($('.fa'))
            .removeClass('fa-close')
            .addClass('fa-bars')
          $('.sideBarText').css('left', '-40.5vw')
        }
      }
    }
  }
})
