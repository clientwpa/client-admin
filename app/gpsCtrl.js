app.controller('gpsCtrl', function (
  $q,
  $window,
  $scope,
  $timeout,
  $rootScope,
  $window,
  $route,
  $routeParams,
  $location,
  $http,
  $anchorScroll,
  toaster
) {
  $rootScope.currentLocation = ''
  $scope.currentLocation = ''

  $scope.locaction = $rootScope.location

  // var xmlHttp;

  // function srvTime() {
  //     try {
  //         //FF, Opera, Safari, Chrome
  //         xmlHttp = new XMLHttpRequest();
  //     } catch (err1) {
  //         //IE
  //         try {
  //             xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
  //         } catch (err2) {
  //             try {
  //                 xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
  //             } catch (eerr3) {
  //                 //AJAX not supported, use CPU time.
  //                 alert("AJAX not supported");
  //             }
  //         }
  //     }
  //     xmlHttp.open('HEAD', window.location.href.toString(), false);
  //     xmlHttp.setRequestHeader("Content-Type", "text/html");
  //     xmlHttp.send('');
  //     return xmlHttp.getResponseHeader("Date");
  // }

  // if ($location.$$host == 'localhost') {
  //     var hostName = $location.$$protocol + '://' + $location.$$host;
  //     var base_url = hostName + '/';
  // } else {
  //     var base_url = $location.$$protocol + '://' + $location.$$host + '/';
  // }

  $scope.loadCurrntOrgPageTitle = function (orgPageTitle) {
    $rootScope.orgPageTitle = orgPageTitle
  }

  $scope.loadMainMenuCurrntActiveClass = function (ActiveClass) {
    $scope.loading = true
    $rootScope.activeClass = ActiveClass
    $scope.loading = false
  }

  function showToast (type, title, value) {
    toaster.pop(type, title, '<b>' + value + '</b>', 5000, 'trustedHtml')
  }

  $scope.latitude
  $scope.longitude

  $scope.loading = true

  var result = $.Deferred()

  $scope.continueToUpdate = function () {
    Swal.fire({
      title: '<strong>GPS Location</strong>',
      icon: 'info',
      html:
        '<div style="font-size: 1.25rem">You are about to assign new location to current unit</div>',
      showCancelButton: true,
      showConfirmButton: true,
      useRejections: true,
      closeOnCancel: true,
      focusConfirm: false,
      confirmButtonText: '<i class="fa fa-check"></i>&nbsp;Go Ahead',
      confirmButtonAriaLabel: 'Go Login',
      cancelButtonText: '<i class="fa fa-times"></i>&nbsp;CANCEL',
      cancelButtonAriaLabel: 'CANCEL'
    }).then(function (isConfirm) {
      console.log('Value: ' + JSON.stringify(isConfirm))
      if (isConfirm.value && isConfirm.value === true) {
        console.log('Continue')
        var serviceBase = 'api/restApi.php?Request='
        /* Save Location */

        var saveLocation =
          '{ "ServiceRequestID": "2", "id": "' +
          $scope.location.id +
          '", "unit_name": "' +
          $scope.location.unit_name +
          '","unit_latitude":"' +
          $scope.latitude +
          '","unit_longitude":"' +
          $scope.longitude +
          '"}'
        console.log('saveLocation: ' + saveLocation)

        saveLocationData = {}

        $.ajax({
          url: serviceBase,
          type: 'POST',
          data: saveLocation,
          // dataType: "json",
          async: false,

          success: function (data, status, xhr) {
            saveLocationData = data.ResponseData ? data.ResponseData : {}
            console.log('saveLocationData: ' + JSON.stringify(saveLocationData))
            $scope.hasAddressData = data.length > 0 ? true : false

            if ($scope.hasAddressData) {
              showToast('info', 'Location Updated', 'Sucessfully')
            } else {
              showToast(
                'info',
                'Location Updated (Address Missing)',
                'Sucessfully'
              )
            }
          },
          error: function (jqXhr, textStatus, errorMessage) {
            appointmentsTasksData = errorMessage + ' - ' + errorMessage
            showToast(
              'error',
              'Error Updating Location ',
              'Error: ' + appointmentsTasksData
            )
          }
        })

        var GoToTaskPage = function () {
          // $location.path('home/client/dashboard/' + unitID + '/' + $scope.customerID + '/' + $scope.unitStatus);
        }

        $timeout(GoToTaskPage, 500)
      } else {
        console.log('Cancel')
        $location.path('home')
      }
    }) // swal
    $scope.loading = false
  }

  function loadScript () {
    console.log('loadScript')

    var script = document.createElement('script')
    script.src =
      '//maps.googleapis.com/maps/api/js?sensor=false&language=en&callback=initMap&key=AIzaSyAxifwnSgSLbU4XFAu-PeKaAO-0v1BWbX8'
    document.body.appendChild(script)
  }

  var deferred = $q.defer()

  $window.initMap = function initMap () {
    $scope.options = {}

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition, showError)
    } else {
      console.log('Geolocation is not supported by this browser.')
      Swal.fire('Geolocation is not supported by this Browser!')
    }

    function showPosition (position) {
      console.log('showPosition called')
      $scope.options = {
        zoom: 17,
        center: {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        }
      }

      $scope.latitude = position.coords.latitude
      $scope.longitude = position.coords.longitude

      $('#latLon').text(
        'Latitude: ' + $scope.latitude + ' - Longitude: ' + $scope.longitude
      )

      var map = new google.maps.Map(
        document.getElementById('map'),
        $scope.options
      )

      console.log(
        'latitude: ' + $scope.latitude + ' - longitude: ' + $scope.longitude
      )

      var marker = {
        coords: {
          lat: $scope.latitude,
          lng: $scope.longitude,
          content: '<b style="color: black">' + 'My Location' + '</b>',
          title: '<b style="color: black">' + 'My Location' + '</b>'
        }
        // iconImage: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
        // 				iconImage: 'https://redwoodrobotics.co.uk/carerapp/img/googlelocation.png',
        // 				content: '<h1>My Current Location</h1>'
      }

      addMarker(marker)

      // Add Marker Function
      function addMarker (props) {
        var marker = new google.maps.Marker({
          position: props.coords,
          map: map
          //icon:props.iconImage
        })

        // Check for customicon
        if (props.iconImage) {
          // Set icon image
          marker.setIcon(props.iconImage)
        }

        // Check content
        if (props.content) {
          var infoWindow = new google.maps.InfoWindow({
            content: props.content
          })

          marker.addListener('click', function () {
            infoWindow.open(map, marker)
          })
        }
      }
    }

    deferred.resolve()

    function showError (error) {
      var errorMessage = ''
      switch (error.code) {
        case error.PERMISSION_DENIED:
          errorMessage = 'User denied the request for Geolocation.'
          break
        case error.POSITION_UNAVAILABLE:
          errorMessage = 'Location information is unavailable.'
          break
        case error.TIMEOUT:
          errorMessage = 'The request to get user location timed out.'
          break
        case error.UNKNOWN_ERROR:
          errorMessage = 'An unknown error occurred.'
      }

      Swal.fire({
        title: '<strong>Location Sevices</strong>',
        icon: 'info',
        html:
          '<div style="font-size: 1.25rem">Location Services Must be Switched-On to use this App!</div><br><br><p>If it is switch on - Please report this error to the Support-Team: (Error) ' +
          errorMessage +
          '</p>',
        showCancelButton: true,
        showConfirmButton: true,
        useRejections: true,
        closeOnCancel: true,
        focusConfirm: false,
        confirmButtonText:
          '<i class="fa fa-check"></i>&nbsp;I HAVE SWITCHED-ON',
        confirmButtonAriaLabel: 'SWITCHED-ON CLICK ME',
        cancelButtonText: '<i class="fa fa-times"></i>&nbsp;CANCEL',
        cancelButtonAriaLabel: 'CANCEL'
      }).then(function (isConfirm) {
        console.log('Value: ' + JSON.stringify(isConfirm))
        if (isConfirm.value && isConfirm.value === true) {
          console.log('Continue After Switching On: route reload')
          $location.path('home')
          // $route.reload();
        } else {
          console.log('Cancel')
          $location.path('home')
        }
      }) // swal
    }
  }
  $(document).ready(function () {
    if ($window.attachEvent) {
      console.log('attachEvent')
      $window.attachEvent('onload', loadScript)
    } else {
      console.log('addEventListener')
      $window.addEventListener('load', loadScript, false)
    }
    loadScript()
  })

  return deferred.promise
})
