app.controller('homeCtrl', function (
  $filter,
  $scope,
  $timeout,
  $rootScope,
  $window,
  $route,
  $routeParams,
  $location,
  $http,
  $anchorScroll,
  toaster
) {
  // Data

  /* Demo Only */
  $scope.demoMode = true;
  $rootScope.demoMode = true;
  /******/

  $rootScope.currentLocation = 'home'
  $scope.currentLocation = 'home'
  $rootScope.lastReadingValue = null
  $scope.selectedFav = $rootScope.selectedFav
  $scope.doNotShowToastMessage = $rootScope.doNotShowToastMessage
  console.log('$scope.selectedFav: ' + $scope.selectedFav);

  $scope.userId = 1
  // clearTimeout($rootScope.doUpdateHistorySim);

  if ($rootScope.location) {
    $scope.location = $rootScope.location
  } else {
    $scope.toggle = false
    $scope.location = {
      unit_id: 0,
      location_id: 1001,
      previousLocation: 0
    }
  }

  // if ($rootScope.clientLoggedIn) {
  // 		$rootScope.userId = $scope.userDetailsObj.UserID;
  // 		$scope.apptStarted = $rootScope.apptStarted;
  // 	} else {
  // 		$location.path('/logout');
  // 	}

  /// var xmlHttp;
  //
  //     function srvTime() {
  //         try {
  //             //FF, Opera, Safari, Chrome
  //             xmlHttp = new XMLHttpRequest();
  //         }
  //         catch (err1) {
  //             //IE
  //             try {
  //                 xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
  //             }
  //             catch (err2) {
  //                 try {
  //                     xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
  //                 }
  //                 catch (eerr3) {
  //                     //AJAX not supported, use CPU time.
  //                     alert("AJAX not supported");
  //                 }
  //             }
  //         }
  //         xmlHttp.open('HEAD', window.location.href.toString(), false);
  //         xmlHttp.setRequestHeader("Content-Type", "text/html");
  //         xmlHttp.send('');
  //         return xmlHttp.getResponseHeader("Date");
  //     }
  //
  //     var st = srvTime();
  //     var serverDate = new Date(st);
  //     $scope.dtmax = serverDate;
  //
  //     $scope.today = function () {
  //         $scope.dt = serverDate;
  //     };
  //     $scope.today();

  $scope.loadCurrntOrgPageTitle = function (orgPageTitle) {
    $rootScope.orgPageTitle = orgPageTitle
  }

  $scope.loadMainMenuCurrntActiveClass = function (ActiveClass) {
    $scope.loading = true
    $rootScope.activeClass = ActiveClass
    $scope.loading = false
  }

  function showToast (type, title, value) {
    toaster.pop(type, title, '<b>' + value + '</b>', 5000, 'trustedHtml')
  }

  $scope.favToggle = function (selectElement) {
    $scope.toggle = !$scope.toggle
    console.log('TOGGLE-CTRL')
  }

  $scope.$watch('location.location_id', function () {
    // console.log('location Obj:' + $scope.location.location_id);
  })

  $scope.doDisplayUnitInfo = function () {
    Swal.fire({
      html:
        'Location ID: ' +
        $scope.location.location_id +
        '<br>' +
        'Unit Name: ' +
        $scope.location.unit_name +
        '<br>' +
        'Unit Address: ' +
        $scope.location.address +
        '<br>' +
        ''
    })
  }

  $scope.doChangeLocation = function (notShowToast) {
    if ($scope.location.previousLocation == $scope.location.location_id) {
      notShowToast = true
    }
    console.log('location Obj:' + $scope.location.location_id)

    var serviceBase = 'api/restApi.php?Request=' // /restApi.php?Request=
    var request =
      '{ "ServiceRequestID": "1", "location": "' +
      $scope.location.location_id +
      '"}'

    $.ajax({
      url: serviceBase,
      type: 'POST',
      // contentType: "application/json; charset=utf-8",
      data: request,
      // dataType: "json",
      async: false,

      success: function (data, status, xhr) {
        data = JSON.parse(data)

        if (data.ResponseData.Data[0]) {
          $scope.hasData = true

          $scope.location = data.ResponseData.Data[0]
          $rootScope.location = $scope.location

          console.log('$scope.location: ' + JSON.stringify($scope.location))
          $scope.location.previousLocation = $scope.location.location_id
          $scope.location.location_id = parseInt($scope.location.location_id)

          if (!notShowToast) {
            showToast('info', 'Location Changed', 'Sucessfully')
          }
        } else {
          $scope.hasData = false
          Swal.fire('Location: ' + $scope.location.location_id + ' Not Found')
          $scope.location.location_id = $scope.location.previousLocation
          console.log('Not Found')
        }

        console.log(
          'selected location profile: ' + JSON.stringify($scope.location)
        )
      },
      error: function (jqXhr, textStatus, errorMessage) {
        console.log(
          'errorMessage: ' +
            JSON.stringify(errorMessage) +
            ' : ' +
            JSON.stringify(jqXhr + ' : ' + JSON.stringify(textStatus))
        )
        return errorMessage
      }
    })
  }

  // {
  //   "id": "31",
  //   "user_id": "1",
  //   "unit_id": "1",
  //   "location_id": "1001",
  //   "unit_name": "Saturn",
  //   "unit_location": "6 N Common Rd, Uxbridge UB8 1PD, UK",
  //   "created_datetime": "2021-11-24 10:34:11"
  // }

  $scope.firstChangeLocation = function () {
    var serviceBase = 'api/restApi.php?Request='
    var request =
      '{ "ServiceRequestID": "1", "location": "' +
      $scope.location.location_id +
      '"}'

    $.ajax({
      url: serviceBase,
      type: 'POST',
      data: request,
      async: false,

      success: function (data, status, xhr) {
        data = JSON.parse(data)

        if (data.ResponseData.Data[0]) {
          $scope.hasData = true

          $scope.location = data.ResponseData.Data[0]
          $rootScope.location = $scope.location
          $scope.location.previousLocation = $scope.location.location_id
          $scope.location.location_id = parseInt($scope.location.location_id)

          $scope.doNotShowToastMessage = true
          $rootScope.doNotShowToastMessage = true
        } else {
          $scope.hasData = false
          Swal.fire('Location: ' + $scope.location.location_id + ' Not Found')
          $scope.location.location_id = $scope.location.previousLocation
        }
      },
      error: function (jqXhr, textStatus, errorMessage) {
        console.log(
          'errorMessage: ' +
            JSON.stringify(errorMessage) +
            ' : ' +
            JSON.stringify(jqXhr + ' : ' + JSON.stringify(textStatus))
        )
        return errorMessage
      }
    })
  }

  if ($scope.location.location_id != '0') {
    $scope.location.location_id = $scope.location.location_id
    $scope.location.previousLocation = $scope.location.location_id
    console.log('Has location_id from Favs Login')
    $scope.doChangeLocation(true)
  } else {
    $scope.location.location_id = 1001
    $scope.location.previousLocation = 1001
    console.log('1st Time Load')
    $scope.firstChangeLocation()
  }

  setTimeout(function () {
    $scope.location.location_id = $scope.location.location_id
    $scope.$apply()
  }, 3000)

  $scope.lastReadingValue = '0000-00-00 00:00:00'
  // $scope.unitReadingsIndex = 0;
  $scope.currentStatus = 'Safe'

  $scope.favIsSelected = false

  /* EXAMPLE OBJECT 

  $scope.currentValues = {
    "DateTime": "01/05/2021 00:00:00",
    "Temperature": '00.0',
    "RelativeHumidity": '00.0',
    "PM": 0,
    "TVOC": 0,
    "CO2": 0,
    "CO": 0,
    "AirPressure": '00.0',
    "Ozone": '00.0',
    "NO2": '00.0',
    "VirusIndex": 0
  };

  */

  var userId = $scope.userId

  $scope.favChangeSelect = function (firstTime) {
    $scope.fav = document.getElementById('favaddRemoveBtn')

    if (!$scope.favIsSelected) {
      $scope.fav.classList.remove('fa-heart-o')
      $scope.fav.classList.add('fa-heart')
      $scope.fav.style.color = 'red'

      $scope.favIsSelected = true
      console.log('favChangeSelect')

      $scope.addFavtoDB()
    } else {
      $scope.fav.classList.remove('fa-heart')
      $scope.fav.classList.add('fa-heart-o')
      $scope.fav.style.color = 'blueviolet'

      $scope.favIsSelected = false
      console.log('favChangeUnSelect')

      $scope.deleteFavfromDB()
    }
  }

  $scope.loadFavFromDB = function (userId) {
    var serviceBase = 'api/restApi.php?Request='
    var request = '{ "ServiceRequestID": "10", "user_id": "' + userId + '"}'

    $.ajax({
      url: serviceBase,
      type: 'POST',
      data: request,
      async: false,

      success: function (data, status, xhr) {
        data = JSON.parse(data)
        console.log('data: ' + JSON.stringify(data))

        if (data.ResponseData) {
          $scope.hasData = true
          $scope.favList = data.ResponseData
          $rootScope.userFavs = $scope.favList

          $scope.favList.forEach((fav, index) => {
            if (fav.location_id == $scope.location.location_id) {
              $scope.fav = document.getElementById('favaddRemoveBtn')
              $scope.fav.classList.remove('fa-heart-o')
              $scope.fav.classList.add('fa-heart')
              $scope.fav.style.color = 'red'

              $scope.favIsSelected = true
              console.log('favChangeSelect')
            }

            $scope.favIsSelected[index] = true
          })
        } else {
          $scope.hasData = false
        }
      },
      error: function (jqXhr, textStatus, errorMessage) {
        console.log(
          'errorMessage: ' +
            JSON.stringify(errorMessage) +
            ' : ' +
            JSON.stringify(jqXhr + ' : ' + JSON.stringify(textStatus))
        )
        return errorMessage
      }
    })

    return false
  }

  $scope.loadFavFromDB(userId)

  $scope.addFavtoDB = function () {
    var unitId = $scope.location.unit_id
    var locationId = $scope.location.location_id
    var unitName = $scope.location.unit_name
    var unitLocation = $scope.location.address

    var message = ''

    var serviceBase = 'api/restApi.php?Request='
    var request =
      '{ "ServiceRequestID": "8", "unit_id": "' +
      unitId +
      '","user_id": "' +
      $scope.userId +
      '","location_id": "' +
      locationId +
      '","unit_name": "' +
      unitName +
      '","unit_location": "' +
      unitLocation +
      '" }'
    //var request = '{ "ServiceRequestID": "8", "unit_id": "' + unitId + '","user_id": "' + $scope.userId + '","location_id": "' + locationId + '"}'; /* Example */

    $.ajax({
      url: serviceBase,
      type: 'POST',
      data: request,
      async: false,

      success: function (data, status, xhr) {
        data = JSON.parse(data)
        console.log('data: ' + JSON.stringify(data))

        if (data.ResponseData && data.ResponseData == true) {
          $scope.hasData = true

          message = data.Message

          console.log('Fav Added: ' + message)

          showToast('success', 'Fav Added', 'Sucessfully')
        } else {
          $scope.hasData = false

          if (data.Message) {
            message = data.Message
          } else {
            message =
              'Error, please try again or notify your support contact if Error contines to happen. Thank You'
          }

          Swal.fire(message)
          console.log('Fav Not Added: ' + message)
        }
      },
      error: function (jqXhr, textStatus, errorMessage) {
        console.log(
          'errorMessage: ' +
            JSON.stringify(errorMessage) +
            ' : ' +
            JSON.stringify(jqXhr + ' : ' + JSON.stringify(textStatus))
        )
        return errorMessage
      }
    })

    return false
  }

  $scope.deleteFavfromDB = function () {
    var unitId = '1'
    var serviceBase = 'api/restApi.php?Request='
    var request =
      '{ "ServiceRequestID": "9", "unit_id": "' +
      unitId +
      '","user_id": "' +
      $scope.userId +
      '"}'
    console.log('request: ' + JSON.stringify(request))
    // var request = '{ "ServiceRequestID": "9", "unit_id": "4", "user_id": "4", "location_id": "4" }'; /* Example */

    $.ajax({
      url: serviceBase,
      type: 'POST',
      data: request,
      async: false,

      success: function (data, status, xhr) {
        data = JSON.parse(data)
        console.log('data: ' + JSON.stringify(data))

        if (data.ResponseData && data.ResponseData == true) {
          $scope.hasData = true

          message = data.Message

          console.log('Fav Removed: ' + message)

          showToast('success', 'Fav Removed', 'Sucessfully')
        } else {
          $scope.hasData = false

          if (data.Message) {
            message = data.Message
          } else {
            message =
              'Error, please try again or notify your support contact if Error contines to happen. Thank You'
          }

          Swal.fire(message)
          console.log('Fav Not Removed: ' + message)
        }
      },
      error: function (jqXhr, textStatus, errorMessage) {
        console.log(
          'errorMessage: ' +
            JSON.stringify(errorMessage) +
            ' : ' +
            JSON.stringify(jqXhr + ' : ' + JSON.stringify(textStatus))
        )
        return errorMessage
      }
    })
    return false
  }

  $scope.doTimeUpdate = function () {
    $rootScope.lastReadingValue = $scope.lastReadingValue
    $scope.$apply()
  }

  $scope.doUpdateHistory = function () {
    $scope.unitReadingsIndex++
    $scope.lastReadingValue = new Date()

    if ($scope.location.unit_id == '0') {
      return
    }
    var serviceBase = 'api/restApi.php?Request='
    var request =
      '{ "ServiceRequestID": "5", "unit_id": "' + $scope.location.unit_id + '"}'

    $.ajax({
      url: serviceBase,
      type: 'POST',
      data: request,
      async: false,

      success: function (data, status, xhr) {
        data = JSON.parse(data)

        if (data.ResponseData.Data[0]) {
          $scope.hasData = true

          $scope.nextReading = data.ResponseData.Data[0]

          console.log('Last Reading: ' + $scope.lastReadingValue)
          console.log('nextReading: ' + JSON.stringify($scope.nextReading))

          let CO2 = $scope.nextReading.CO2
          let temperature = $scope.nextReading.Temperature
          let relativeHumidity = $scope.nextReading.Relative_Humidity

          if (CO2 >= 1000 && CO2 < 2500) {
            $scope.currentStatus = 'CO2: ' + CO2
          } else if (CO2 >= 2500) {
            $scope.currentStatus = 'CO2: ' + CO2
          } else if (temperature >= 27 && temperature < 30) {
            $scope.currentStatus = 'Temperature: ' + temperature
          } else if (temperature > 30) {
            $scope.currentStatus = 'Temperature: ' + temperature
          } else if (relativeHumidity >= 50 && relativeHumidity < 65) {
            $scope.currentStatus = 'Humidity: ' + relativeHumidity
          } else if (relativeHumidity >= 65) {
            $scope.currentStatus = 'Humidity: ' + relativeHumidity
          } else {
            $scope.currentStatus = 'Safe'
          }
        } else {
          $scope.hasData = false

          console.log('nextReading Not Found')
        }
      }
    })

    $scope.currentDateTime = $filter('date')(
      $scope.lastReadingValue,
      'dd/MM/yyyy HH:mm'
    )

    $scope.doTimeUpdate()

    $rootScope.doUpdateHistory = setTimeout(function () {
      $scope.doUpdateHistory()
    }, 10000)
  }

  setTimeout(function () {
    $scope.doUpdateHistory()
  }, 3000)

  /* FUNCTIONS START*/

  $scope.scrollToFindus = function (id) {
    console.log('id: ' + id)
    $location.path('/contactus')
    $location.hash(id)

    setTimeout(function () {
      $anchorScroll()
    }, 1000)
  }

  /* FUNCTIONS END */
})

app.run(function ($rootScope, $route, $location) {
  // Data
  $rootScope.$on('$routeChangeStart', function (event, next, current) {
    $rootScope.baseUrl = 'https://rejuvenair.app/home'
  })
})

app.directive('marqueeLeft', [
  function () {
    return {
      link: function (scope, element, attrs, ctrl) {
        var marqueeLeft = $(element).liMarquee({
          circular: false,
          width: 'auto',
          height: 'auto',
          hoverStop: false,
          stopOutScreen: true
        })
      },
      restrict: 'A'
    }
  }
])

app.directive('datepicker', function () {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, elem, attrs, ngModelCtrl) {
      var updateModel = function (dateText) {
        scope.$apply(function () {
          ngModelCtrl.$setViewValue(dateText)
        })
      }
      var options = {
        dateFormat: 'dd/mm/yy',
        onSelect: function (dateText) {
          updateModel(dateText)
        }
      }
      elem.datepicker(options)
    }
  }
})

//text trim
app.filter('cut', function () {
  return function (value, wordwise, max, tail) {
    if (!value) return ''

    max = parseInt(max, 10)
    if (!max) return value
    if (value.length <= max) return value

    value = value.substr(0, max)
    if (wordwise) {
      var lastspace = value.lastIndexOf(' ')
      if (lastspace !== -1) {
        //Also remove . and , so its gives a cleaner result.
        if (
          value.charAt(lastspace - 1) === '.' ||
          value.charAt(lastspace - 1) === ','
        ) {
          lastspace = lastspace - 1
        }
        value = value.substr(0, lastspace)
      }
    }

    return value + (tail || ' �')
  }
})