function connectToBle() {
  // Connect to a device by passing the service UUID
  //blueTooth.connect(0x2A19, gotCharacteristics);

 // All Devices
 navigator.bluetooth.requestDevice({
  // filters: [...] <- Prefer filters to save energy & show relevant devices.
     acceptAllDevices: true
   })
   .then(device => {
     console.log('> Requested ' + device.name + ' (' + device.id + ')');
   })
   .catch(error => {
    console.log('Argh! ' + error);
   });

}


// A function that will be called once got characteristics
function gotCharacteristics(error, characteristics) {
  if (error) {
    console.log('error: ', error);
  }
  console.log('characteristics: ', characteristics);

  blueToothCharacteristic = characteristics[0];

  blueTooth.startNotifications(blueToothCharacteristic, gotValue, 'string');

  connectButton.hide();

  startMillis = millis();//use this to ping the sensor
  isConnected = blueTooth.isConnected();
  // Add a event handler when the device is disconnected
  blueTooth.onDisconnected(onDisconnected);
}


// A function that will be called once got values
// received as one single string
// then split by the comma (,)

function gotValue(value) {

  // console.log('value: ', value);
  let splitString = value.split(',');
  if (splitString[0] == 'first') {
    firstValue = splitString[1];
    // console.log('firstValue');
  }
  if (splitString[0] == 'second') {
    secondValue = splitString[1];
  }
  if (splitString[0] == 'third') {
    thirdValue = splitString[1];
  }
  if (splitString[0] == 'forth') {
    forthValue = splitString[1];
  }

  doCalculations();

}

function doCalculations() {
 
  if ( firstValue != '0' && thirdValue != '0.00') {

    timeImpactinSeconds = parseFloat(thirdValue).toFixed(4);
    wallStrikeHeightinFeet = parseFloat(firstValue);

    [velocityX, velocityY] = findVelXY(timeImpactinSeconds, distToWallinFeet, wallStrikeHeightinFeet, initialHeightinFeet);
    // console.log('velocityX: ' + velocityX.toFixed(2), 'velocityY: ' + velocityY.toFixed(2));
    RangeinFeet = maxRange(gravity, velocityX, velocityY, initialHeightinFeet);
    rangeinYards = (RangeinFeet / 3).toFixed(1);
    // console.log(`Estimated range is: ${RangeinFeet} Feet`);
    console.log(`Estimated range is: ${rangeinYards} Yards`);
    console.log(`Estimated range + Carry: ${parseFloat(rangeinYards) + (parseFloat(rangeinYards) * 0.25)} Yards`);

  }

}

function onDisconnected() {
  connectButton.show();
  console.log('Device got disconnected.');
  isConnected = false;
  firstValue = '0';
  secondValue = '0';
  thirdValue = '0.00';
  forthValue = '0.00';
}


function sendData(command) { /* Could be this area */

  // return; /* Simulation Testing Only */

  const inputValue = command;
  if (!("TextEncoder" in window)) {
    console.log("Sorry, this browser does not support TextEncoder...");
  }
  var enc = new TextEncoder(); // always utf-8
  if (blueToothCharacteristic) {
    blueToothCharacteristic.writeValue(enc.encode(inputValue));
  }
  
}

