function drawScreen() {
  //console.log('drawScreen');

  /* production inputs */
  let distToWallinFeet = 6;

  textFont('Orbitron');

  textSize(15);
  background(255);
  textAlign(LEFT, TOP);

  // text('Simulation - Please Tee off Ball ' + distToWallinFeet + ' ft from Mat', 15, 15);

  // defaultCanvas0 
  
  // Simulation only
 
  // forthValue = '0.00'; 
  // isConnected = true;

  /************************ IN CORRECT 
   * 
   * IF BALL IS OFF TEE DISPLAY RESULTS 
   * IF BALL OFF TEE, JUST STATUS UPDATE
   * LEAVE OTHER VALUES
   * 
  */

  if (isConnected) {

    text('Connected', 100, 50);
    
    if (forthValue == '0.01') {
      ballOnTee = true;
      // text("Status: " + 'Ready to Tee Off', 15, 160);
    } else {
      ballOnTee = false;
      // text("Status: " + 'Please place ball back on Tee to Start/Reset', 15, 160);
    }

  
    fithValue = launchAngle + " deg";
    sixthValue = (velocity).toFixed(2) + " ft/s";

    seventhValue = velocityX.toFixed(2) + ' / ' + velocityY.toFixed(2) + " ft/s";
    eighthValue = flightTime + ' secs';
    ninthValue = rangeinYards + " yards";
   
  } else {
    // text('Disconnected', 100, 50);
  }

  // text('Readings From Board', 15, 90);
  // text("Vertical: " + firstValue + " - " + "Horizontal : " + secondValue, 15, 120);
  // text("Time @ Impact: " + thirdValue, 15, 140);

  // line(0, 200, 300, 200);

  // text("Last Result", 15, 220);
  // text("Launch Angle: " + fithValue, 15, 240);
  // text("Velocity/Apex: " + sixthValue, 15, 260);
  // text("Velocity X / Y: " + seventhValue, 15, 280);
  // text("Flight Time: " + eighthValue, 15, 300);
  // text("Estimated Range: " + ninthValue, 15, 320);

  currentMillis = millis();

  if (currentMillis - startMillis >= period) {

    sendData("connected\n");
    startMillis = millis();
  }
}

