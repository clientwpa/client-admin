<!DOCTYPE html>
<html lang="en" ng-app="myApp">
  <?php 
    session_start();
    include 'api/config.php'; 
  ?>
  <head>
    <?php //echo '<pre>'; print_r($_SERVER['REQUEST_URI']); echo '</pre>';
     /* if($_SESSION['UserAccess'] == 'ADMIN') {
        //header("Location: ".URL."organisation");
      }*/
    ?>
    <base href="https://redwoodrobotics.co.uk/carerapp/" />
    
    <!-- New -->
    <link rel="manifest" href="manifest.json">
    <link rel="icon" type="image/png" sizes="32x32" href="img/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/icons/favicon-16x16.png">
    
    <meta name="theme-color" content="#4DBA87">
    <meta name="apple-mobile-web-app-capable" content="no">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="apple-mobile-web-app-title" content="Smarter Care Plans">
    <meta name="msapplication-TileImage" content="img/icons/msapplication-icon-144x144.png">
    <meta name="msapplication-TileColor" content="#000000">
    
    <link rel="apple-touch-icon" href="img/icons/apple-touch-icon-152x152.png">
    <link rel="mask-icon" href="img/icons/safari-pinned-tab.svg" color="#4DBA87">
    <!-- *** -->
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"">
    <link rel="shortcut icon" href="<?php echo URL; ?>images/favicon.png">
    <link href="<?php echo URL; ?>css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="<?php echo URL; ?>css/bootstrap-theme.css" rel="stylesheet">
    <link href="<?php echo URL; ?>css/font-awesome.min.css" rel="stylesheet" />
    <!-- owl carousel -->
    <link href="<?php echo URL; ?>css/style.css" rel="stylesheet">
    <!--link href="<?php echo URL; ?>css/new_style.css" rel="stylesheet"-->
	  <!--For sidebar-->
	  <link href="<?php echo URL; ?>css/new_style2.css" rel="stylesheet">
    <link href="<?php echo URL; ?>css/style-responsive.css" rel="stylesheet" />
    <link href="<?php echo URL; ?>css/jquery-ui-1.10.4.min.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?php echo URL; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo URL; ?>css/animate.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo URL; ?>css/custom.css" rel="stylesheet">
    <link href="<?php echo URL; ?>css/carerapp.css" rel="stylesheet">
   
    <link href="<?php echo URL; ?>css/toaster.css" rel="stylesheet">
	  <link rel="stylesheet" href="<?php echo URL; ?>css/datepicker.css">
    <!-- Form Builder Style -->
    <link href="<?php echo URL; ?>assets/form-builder/site.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo URL; ?>assets/form-builder/angular-form-builder.css">
    <link rel='stylesheet prefetch' href='<?php echo URL; ?>css/dx.spa.css'>
    <link rel='stylesheet prefetch' href='<?php echo URL; ?>css/dx.common.css'>
    <link rel='stylesheet prefetch' href='<?php echo URL; ?>css/dx.light.css'>
    <link rel="stylesheet" href="<?php echo URL; ?>assets/alertifyjs/ng-alertify.css" />
  </head>

  <body ng-cloak="" scroll="no">
    <?php //$uri =  explode('/', $_SERVER['REQUEST_URI']); ?>
    <?php //if($uri[2] == '') { ?>
    <div id="preloader">
      <div id="status">&nbsp;</div>
    </div>
    <?php //} ?>
    
    <div class="containers">

      <div data-ng-view="" id="ng-view" class="slide-animation"></div>

    </div>
  </body>
  <toaster-container toaster-options="{'time-out': 3000}"></toaster-container>
  <!-- javascripts -->
  
  <script src="<?php echo URL; ?>js/jquery.js"></script>
  <script src="<?php echo URL; ?>js/jSignature.min.js"></script>

  <script src="<?php echo URL; ?>assets/scheduler/js/cldr.min.js"></script>
  <script src="<?php echo URL; ?>assets/scheduler/js/cldr/event.min.js"></script>
  <script src="<?php echo URL; ?>assets/scheduler/js/cldr/supplemental.min.js"></script>
  <script src="<?php echo URL; ?>assets/scheduler/js/cldr/unresolved.min.js"></script>  
  <script type="text/javascript" src="<?php echo URL; ?>assets/scheduler/js/globalize.min.js"></script>
  <script type="text/javascript" src="<?php echo URL; ?>assets/scheduler/js/globalize/message.min.js"></script>
  <script type="text/javascript" src="<?php echo URL; ?>assets/scheduler/js/globalize/number.min.js"></script>
  <script type="text/javascript" src="<?php echo URL; ?>assets/scheduler/js/globalize/currency.min.js"></script>
  <script type="text/javascript" src="<?php echo URL; ?>assets/scheduler/js/globalize/date.min.js"></script>

  <script src="<?php echo URL; ?>js/angular.min.js"></script>
  <script src="<?php echo URL; ?>js/angular-sanitize.min.js"></script>
  
<!--   <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular-sanitize.min.js"></script> -->
  
<!--  <link rel="stylesheet" href="http://mbenford.github.io/ngTagsInput/css/ng-tags-input.min.css" />
  <script src="http://mbenford.github.io/ngTagsInput/js/ng-tags-input.min.js"></script>-->
  <script src="<?php echo URL; ?>js/angular-route.min.js"></script>
  <script src="<?php echo URL; ?>js/angular-animate.min.js" ></script>
  <script src="<?php echo URL; ?>js/toaster.js"></script>
  <script src="<?php echo URL; ?>app/app.js"></script>
  <script src="<?php echo URL; ?>app/data.js"></script>
  <script src="<?php echo URL; ?>app/directives.js"></script>
  <script src="<?php echo URL; ?>app/authCtrl.js"></script>
  <script src="<?php echo URL; ?>app/orgCtrl.js"></script>
  <script src="<?php echo URL; ?>app/clientCtrl.js"></script>
  <script src="<?php echo URL; ?>app/carerCtrl.js"></script>
  <script src="<?php echo URL; ?>app/carerNotesCtrl.js"></script>
  <script src="<?php echo URL; ?>app/tasksCtrl.js"></script>
  <script src="<?php echo URL; ?>app/gpsCtrl.js"></script>
  <script src="<?php echo URL; ?>app/qrCtrl.js"></script>
  <script src="<?php echo URL; ?>app/displayMessagesCtrl.js"></script>
  <script src="<?php echo URL; ?>app/nfcCtrl.js"></script>
  <script src="<?php echo URL; ?>app/taskConfirmCtrl.js"></script>
  <script src="<?php echo URL; ?>app/displayFormCtrl.js"></script>
  <script src="<?php echo URL; ?>app/clientPlansCtrl.js"></script>
  <script src="<?php echo URL; ?>app/clientSearchCtrl.js"></script>
  <script src="<?php echo URL; ?>app/moreApptDetailsCtrl.js"></script>
  <script src="<?php echo URL; ?>app/carermenuCtrl.js"></script>
  <script src="<?php echo URL; ?>app/clientFormsCtrl.js"></script>
  <script src="<?php echo URL; ?>app/clientFormsEditCtrl.js"></script>
  <script src="<?php echo URL; ?>app/unattendedApptCtrl.js"></script>
  <script src="<?php echo URL; ?>app/orgDashboardCtrl.js"></script>
  <script src="<?php echo URL; ?>assets/datatable/angular-datatables.min.js"></script>
  <script src="<?php echo URL; ?>assets/datatable/jquery.dataTables.min.js"></script>
  <script src="<?php echo URL; ?>assets/datatable/dirPagination.js"></script>
  
<!--   <script src="<?php echo URL; ?>js/bootstrap.js"></script> -->
  <script src="<?php echo URL; ?>js/bootstrap_4.5.0.min.js"></script>
  <script src="<?php echo URL; ?>js/popper.js"></script>
  
  <script src="<?php echo URL; ?>js/jquery-ui-1.9.2.custom.min.js"></script>
  <!-- nice scroll -->
  <script src="<?php echo URL; ?>js/jquery.scrollTo.min.js"></script>
  <script src="<?php echo URL; ?>js/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="<?php echo URL; ?>js/scripts.js"></script>
  <script src="<?php echo URL; ?>js/bootstrap-datepicker.js"></script>
  <script src="<?php echo URL; ?>js/moment-min.js"></script>
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.7.0/lodash.min.js"></script>-->
  <!-- Libs -->
  <script>
      // makes sure the whole site is loaded
      $(window).load(function() {
              // will first fade out the loading animation
        $("#status").fadeOut();
              // will fade out the whole DIV that covers the website.
        $("#preloader").delay(100).fadeOut("slow");
      });
  </script>
  <script src="<?php echo URL; ?>assets/form-builder/angular-form-builder.js"></script>
  <script src="<?php echo URL; ?>assets/form-builder/angular-form-builder-components.js"></script>
  <script type="text/javascript" src="<?php echo URL; ?>js/angular-validator.min.js"></script>
  <script type="text/javascript" src="<?php echo URL; ?>js/angular-validator-rules.min.js"></script>
  <script src="<?php echo URL; ?>js/ui-bootstrap-tpls.js"></script>
  <script src="<?php echo URL; ?>js/angular-ui-bootstrap-modal.js"></script>
  <script src='<?php echo URL; ?>js/dx.all.js'></script>
  <script src='<?php echo URL; ?>app/bootstrap-multiselect.js'></script>
  <script src="<?php echo URL; ?>assets/alertifyjs/ng-alertify.js"></script>
  <script src="<?php echo URL; ?>assets/ckeditor/ckeditor.js"></script> 
  <script src="<?php echo URL; ?>js/sweetalert2.all.min.js"></script>
  		<script src="<?php echo URL; ?>assets/angularpichart/Chart.js"></script>
        <script src="<?php echo URL; ?>assets/angularpichart/angular-chart.js"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo URL; ?>assets/angularpichart/angular-chart.css">  

  <script type="text/javascript">
  
  if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {
    navigator.serviceWorker.register('service-worker.js').then(function(registration) {
      // Registration was successful
      console.log('ServiceWorker registration successful with scope: ', registration.scope);
    }, function(err) {
      // registration failed :(
      console.log('ServiceWorker registration failed: ', err);
    });
  });
}

    function getfocus() {
  		//document.getElementById("fname").focus();
  		document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
  	}	
	function getfocustask(){
  		document.getElementById("TaskDesc").focus();
  		document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;	
	}
	function getfocusscroll(){
  		document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;	
	}
  	function removeAllergies(id){
	   if(confirm("Are you sure you want to Delete?")){
  	   $('#remove_'+id).remove();
	   }
  	}
  </script>
</html>
