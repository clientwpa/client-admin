<?php

date_default_timezone_set("Europe/London'");
require_once("PHPMailer/PHPMailerAutoload.php");
require_once("restInc.php");
require_once("config.php");

class API extends REST
{

    public $data = "";

    const DB_SERVER = DB_SERVER;
    const DB_USER = DB_USER;
    const DB_PASSWORD = DB_PASSWORD;
    const DB = DB;
    const img = img;
    const base_url = base_url;
    const URL = URL;
    const APPLICATION_NAME = APPLICATION_NAME;
    const FROMEMAIL = FROMEMAIL;

    private $db = NULL;

    public function __construct()
    {
        parent::__construct();    // Init parent contructor
        $this->dbConnect();     // Initiate Database connection   
    }

    private function dbConnect()
    {
        $this->db = mysql_connect(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD);
        if ($this->db)
            mysql_select_db(self::DB, $this->db);

        if (!$this->db) {
            $dbError = array();
            $dbError['status_code'] = "0";
            $dbError['errorCode'] = "8";
            $this->response($this->json($dbError), 400);
        }
    }

    /*
     * Public method for access api.
     * This method dynmically call the method based on the query string
     *
     */

    public function processApi()
    {
        $func = strtolower(trim(str_replace("/", "", $_REQUEST['request'])));
        if ((int) method_exists($this, $func) > 0)
            $this->$func();
        else
            $this->response('', 404);    // If the method not exist with in this class, response would be "Page not found".
    }

    /** Functions **/

    private function json($data)
    {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    /** SYSTEM FUNCTIONS **/

    function Login()
    {
        if ($this->get_request_method() != "POST") {
            $error = array('status_code' => "0", 'message' => "wrong method!", 'response_code' => "406");
            $this->response($this->json($error), 406);
        }

        $arr = array();
        if (@$_POST['reqparams']) {
            $post = $_POST['reqparams'];
            $username = $post['username'];
            $password = $post['password'];
        } else {
            $username = $_POST['username'];
            $password = $_POST['password'];
        }

        $success = true;

        // VALIDATION CHECK
        if (empty($username)) {
            $success = false;
            $error = array('status_code' => "0", 'message' => "Please enter username", 'response_code' => "200", 'response_data' => $arr);
            $this->response($this->json($error), 200);
        }

        // LOGIN VALIDATION CHECK ENDS
        if ($success) {
            // LOGIN

            // md5($password)
            $sql = mysql_query("SELECT * FROM `RJN_UserLogin` WHERE UserName='$username' AND Password='" . $password . "'", $this->db);
            $arr = array();
            if (mysql_num_rows($sql) > 0) {
                while ($rlt = mysql_fetch_array($sql, MYSQL_ASSOC)) {
                    $row['UserID'] = $rlt['UserID'];
                    $row['FirstName'] = $rlt['FirstName'];
                    $row['LastName'] = $rlt['LastName'];
                    $row['UserName'] = $rlt['UserName'];
                    $row['EmailID'] = $rlt['EmailID'];
                    $row['StatusID'] = $rlt['StatusID'];
                    $arr = $row;
                }
                $UserID = $arr['UserID'];

                $sql1 = mysql_query("SELECT * FROM `RJN_UserAccess` WHERE UserID='$UserID'", $this->db);

                // $careOrgSql = "SELECT * FROM `RJN_CareOrg` WHERE OrgID='" . $OrgID . "'";
                // $resultOrgSql = mysql_query($careOrgSql) or die(mysql_error());
                // $rowOrgs = mysql_fetch_assoc($resultOrgSql);
                // if (mysql_num_rows($resultOrgSql) > 0) {
                //     $OrgName = $rowOrgs['Name'];
                //     $OrgNumber = $rowOrgs['ContactNo'];
                // } else {
                //     $OrgName = '';
                //     $OrgNumber = '';
                // }

                $arr2 = array();
                $UserType = '0';
                if (mysql_num_rows($sql1) > 0) {
                    while ($rlt = mysql_fetch_array($sql1, MYSQL_ASSOC)) {
                        $row['UserAccessID'] = $rlt['UserAccessID'];
                        $row['UserType'] = $rlt['UserType'];
                        $row['AccessLevel'] = $rlt['AccessLevel'];
                        $arr2 = $row;
                    }
                    $UserType = $arr2['UserType'];
                }
                $results = array_merge($arr, $arr2);
                if ($arr['StatusID'] == '0') {
                    $successData = array('status_code' => "0", 'status' => "error", 'message' => "Your account is disabled. Please contact your administrator", 'response_code' => "200", 'response_data' => $arr);
                    $this->response($this->json($successData), 200);
                }

                $FullName = $arr['FirstName'] + ' ' + $arr['LastName'];
                $successData = array('status_code' => "1", 'status' => "success", 'message' => "Login Successful", 'fullName' => $FullName, 'response_code' => "200", 'response_data' => $results);
                $this->response($this->json($successData), 200);
                return;

                if (!isset($_SESSION)) {
                    session_start();
                }

                $_SESSION['UserID'] = $arr['UserID'];
                $_SESSION['EmailID'] = $arr['EmailID'];
                $_SESSION['UserName'] = $arr['UserName'];
                $_SESSION['ProfilePhoto'] = $arr['ProfilePhoto'];
                $_SESSION['DashboardLogo'] = $arr['DashboardLogo'];
                $_SESSION['UserTypeID'] = $UserTypeID;

                if ($UserTypeID == 2 || $UserTypeID == 3) {
                    $_SESSION['OrgID'] = $careOrgSql['OrgID'];
                    if ($UserTypeID == 2) {
                        $_SESSION['MessageName'] = $careOrgSql['Name'];
                    } elseif ($UserTypeID == 3) {
                        $StaffSql = mysql_query("SELECT * FROM `RJN_Staff` WHERE UserID='$UserID'", $this->db);
                        $StaffSql = mysql_fetch_array($StaffSql, MYSQL_ASSOC);
                        $_SESSION['MessageName'] = $StaffSql['Name'] . ' ' . $StaffSql['Surname'];
                    }
                    $_SESSION['AdminName'] = $careOrgSql['Name'];
                    $_SESSION['CareOrgProfilePhoto'] = $arr['ProfilePhoto'];
                    $_SESSION['CareOrgDashboardLogo'] = $arr['DashboardLogo'];
                } else {
                    $_SESSION['OrgID'] = '';
                    $_SESSION['AdminName'] = '';
                    $_SESSION['MessageName'] = $arr['UserName'];
                    $_SESSION['CareOrgProfilePhoto'] = '';
                    $_SESSION['CareOrgDashboardLogo'] = '';
                }

                $UserTypeSql = mysql_query("SELECT * FROM `RJN_UserType` WHERE UserTypeID='$UserTypeID'", $this->db);
                $UserType = mysql_fetch_array($UserTypeSql, MYSQL_ASSOC);
                $_SESSION['UserAccess'] = $UserType['UserTypeName'];

                $successdata = array('status_code' => $UserTypeID, 'status' => "success", 'message' => "Login Successful", 'response_code' => "200", 'response_data' => $arr);
                $this->response($this->json($successdata), 200);
            } else {
                //if no user found
                $error = array('status_code' => "0", 'status' => "error", 'message' => "Wrong username and password!!!", 'response_code' => "200", 'response_data' => $arr);
                $this->response($this->json($error), 200);
            }
        } else {
            $error = array('status_code' => "0", 'status' => "error", 'message' => "validation error", 'response_code' => "200", 'response_data' => $arr);
            $this->response($this->json($error), 200);
        }
    }



    function ForgotPassword()
    {
        if ($this->get_request_method() != "POST") {
            $error = array('status_code' => "0", 'message' => "wrong method", 'response_code' => "406");
            $this->response($this->json($error), 406);
        }
        $arr = array();
        if (@$_POST['reqparams']) {
            $post = $_POST['reqparams'];
            $username = $post['username'];
        }
        $success = true;
        if ($success) {
            // LOGIN
            $sql = mysql_query("SELECT UL.*, UA.UserTypeID, UA.StatusID as UAStatusID, CO.StatusID as COStatusID, L.StatusID as LStatusID, LP.StatusID as LPStatusID
                FROM RJN_UserLogin as UL
                LEFT JOIN RJN_UserAccess as UA ON UA.UserID=UL.UserID
                LEFT JOIN RJN_CareOrg as CO ON CO.UserID=UL.UserID
                LEFT JOIN RJN_Licenses as L ON L.UserID=UL.UserID
                LEFT JOIN RJN_LicensesPlan as LP ON LP.PlanID=CO.PlanID
                WHERE UserName='$username'
                ", $this->db);
            if (mysql_num_rows($sql) > 0) {
                $userData = mysql_fetch_array($sql, MYSQL_ASSOC);
                //print_r($userData); die();
                $UserTypeID = $userData['UserTypeID'];
                $UserID = $userData['UserID'];
                /*if($UserTypeID == 4 || $UserTypeID == 5 || $UserTypeID == 6) {
                    $successdata = array('status_code' => "0", 'status' => "error", 'message' => "You are not authorized to access web portal.", 'response_code' => "200", 'response_data' => $arr);
                    $this->response($this->json($successdata), 200);
                } 
                if($userData['StatusID'] != '1') {
                    $successdata = array('status_code' => "0", 'status' => "error", 'message' => "Your account is disabled. Please contact your administrator", 'response_code' => "200", 'response_data' => $arr);
                    $this->response($this->json($successdata), 200);
                }                 
                if (empty($userData['LStatusID']) || $userData['LStatusID'] != '1') {
                    $error = array('status_code' => "0", 'status' => "error", 'message' => "Your license is disabled. Please contact your administrator", 'response_code' => "200", 'response_data' => $arr);
                    $this->response($this->json($error), 200);
                }
                if (empty($userData['LPStatusID']) || $userData['LPStatusID'] != '1') {
                    $error = array('status_code' => "0", 'status' => "error", 'message' => "Your license plan is disabled. Please contact your administrator", 'response_code' => "200", 'response_data' => $arr);
                    $this->response($this->json($error), 200);
                }*/
                $encryptuserid = $userData['UserID'] . $this->generateRandomString();
                mysql_query("UPDATE `RJN_UserLogin` SET `ResetKey`='$encryptuserid' WHERE `UserID` = '$UserID'", $this->db);
                $name = ucwords($userData['UserName']);
                $email = $userData['EmailID'];
                $reset_link = URL . 'resertpassword/' . $encryptuserid;
                $current_year = date('Y');
                $logopath = URL . 'images/Logo.png';
                //echo '<pre>'; print_r( $template_details ); 
                $message = '<body style="margin:0; padding:0; font-size:15px; font-family:Arial, Helvetica, sans-serif; color:#666;">  
                      <table align="center" cellspacing="0" cellpadding="0" width="600" border="0" style="padding:0; background-size:100% 100%; background-color:#f2f2f2;">
                        <tr>
                          <td style="padding:0 30px 10px;">
                              <table cellspacing="0" cellpadding="0" width="100%" border="0" style="text-align:center;">
                                <tr>
                                  <td><img src="' . $logopath . '" alt="' . APPLICATION_NAME . '" style="width:38%;padding: 30px 1px;"></td>
                                </tr>
                              </table>
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                              <td style="background:#fff;text-align:left; padding:25px 30px; border-radius:10px;">
                              <table width="100%" style="margin:0;">
                                <tr>
                                  <td style="padding:8px 0; font-size:15px; font-family:Arial, Helvetica, sans-serif;">
                                    <p style="margin:0; padding:0;">Hello ' . $name . ',</p>
                                  </td>
                                </tr>
                                <tr>
                                  <td style="padding:8px 0; font-family:Arial, Helvetica, sans-serif;">    
                                    <p style="margin:0;">We received your request to reset your password.</p>
                                  </td>
                                </tr>
                                <tr>
                                  <td style="padding:8px 0; font-family:Arial, Helvetica, sans-serif;">    
                                    <p style="font-size:15px; color:#333; font-weight:500; margin:0;">Please click the link below: <br> <a href="' . $reset_link . '" target="_blank">Click here to reset your password</a></p>
                                  </td>
                                </tr>
                                <tr>
                                  <td style="padding:8px 0; font-family:Arial, Helvetica, sans-serif;">    
                                    <p style="margin:0;">If you have any questions or trouble during the password reset process, email us at <a href="mailto:' . FROMEMAIL . '">' . FROMEMAIL . '</a>.</p>
                                  </td>
                                </tr>
                                <tr>
                                  <td style="padding:15px 0 5px; font-family:Arial, Helvetica, sans-serif;">
                                    <p style="margin:0; font-size:13px; font-family:Arial, Helvetica, sans-serif;">Thank You, <br>' . APPLICATION_NAME . ' Team.</p>
                                  </td>
                                </tr>
                              </table>
                              </td>
                              </tr>
                              </table>
                              <table cellspacing="0" cellpadding="0" width="100%" border="0" style="text-align:center; padding:0; border-radius:10px 10px 0 0; margin:0;font-size:12px;">
                                <tr>
                                    <td style="padding:15px 0;"><p style="margin:0; font-family:Arial, Helvetica, sans-serif;">Copyright &copy; ' . $current_year . ' ' . APPLICATION_NAME . '. All Rights Reserved.</p></td>
                                </tr>
                              </table>
                          </td>
                        </tr>  
                      </table>
                    </body>';
                //$send = "1";
                //@$send = $this->sendMail(FROMEMAIL, $email, APPLICATION_NAME." - Reset Password Link", $message, APPLICATION_NAME);
                @$send = $this->sendEmail(FROMEMAIL, APPLICATION_NAME, $email, $email, APPLICATION_NAME . " - Reset Password Link", $message);
                //echo $result.'===';
                if ($send['SUCCESS'] == 'TRUE') {
                    $successdata = array('status_code' => '1', 'status' => "success", 'message' => "Password reset link sent on registered email", 'response_code' => "200", 'response_data' => $arr);
                    $this->response($this->json($successdata), 200);
                } else {
                    $error = array('status_code' => "0", 'status' => "error", 'message' => 'Error encountered while send mail.', 'response_code' => "200", 'response_data' => $arr);
                    $this->response($this->json($error), 200);
                }
            } else {
                $error = array('status_code' => "0", 'status' => "error", 'message' => "Wrong username!!!", 'response_code' => "200", 'response_data' => $arr);
                $this->response($this->json($error), 200);
            }
        } else {
            $error = array('status_code' => "0", 'status' => "error", 'message' => "validation error", 'response_code' => "200", 'response_data' => $arr);
            $this->response($this->json($error), 200);
        }
    }

    function doResetPassword()
    {
        if ($this->get_request_method() != "POST") {
            $error = array('status_code' => "0", 'message' => "wrong method", 'response_code' => "406");
            $this->response($this->json($error), 406);
        }
        if (@$_POST['reqparams']) {
            $post = $_POST['reqparams'];
            $password = md5($post['npassword']);
        }
        $key = $_POST['key'];
        if ($key) {
            $sql = mysql_query("SELECT * FROM `RJN_UserLogin` WHERE ResetKey='$key'", $this->db);
            $userdata = mysql_fetch_array($sql, MYSQL_ASSOC);
            $UserID = $userdata['UserID'];
            mysql_query("UPDATE `RJN_UserLogin` SET `ResetKey`='', `password`='$password' WHERE `UserID` = '$UserID'", $this->db);
        }
        $successdata = array('status_code' => "1", 'status' => "success", 'message' => 'Password changed successfully', 'response_code' => "200", 'response_data' => '');
        $this->response($this->json($successdata), 200);
    }

    function checkResetKey()
    {
        if ($this->get_request_method() != "POST") {
            $error = array('status_code' => "0", 'message' => "wrong method", 'response_code' => "406");
            $this->response($this->json($error), 406);
        }
        $key = $_POST['key'];
        if ($key) {
            $sql = mysql_query("SELECT * FROM `RJN_UserLogin` WHERE ResetKey='$key'", $this->db);
            if (mysql_num_rows($sql) > 0) {
                echo 'yes';
            } else {
                $error = array('status_code' => "0", 'status' => "error", 'message' => "Link has expired", 'response_code' => "200", 'response_data' => '');
                $this->response($this->json($error), 200);
            }
        }
    }

    function checkLoginDetails()
    {

        if ($this->get_request_method() != "POST") {
            $error = array('status_code' => "0", 'status' => "error", 'message' => "wrong method", 'response_code' => "406");
            $this->response($this->json($error), 406);
        }

        if (@$_POST['reqparams']) {
            $post = $_POST['reqparams'];
        } else {
            $error = array('status_code' => "0", 'status' => "error", 'message' => "reqparams error", 'response_code' => "200", 'response_data' => $arr);
            $this->response($this->json($error), 200);
        }

        $success = true;

        $studentNumber = $post['studentnumber'];

        // VALIDATION CHECK
        if (empty($studentNumber)) {
            $success = false;
            $error = array('status_code' => "0", 'message' => "Please enter student number", 'response_code' => "200", 'response_data' => "Empty Student Number");
            $this->response($this->json($error), 200);
        }

        // LOGIN VALIDATION CHECK ENDS
        if ($success) {
            // LOGIN
            $sql = mysql_query("SELECT * FROM `student_login` WHERE student_number='$studentNumber'", $this->db);

            $row = array();
            if (mysql_num_rows($sql) > 0) {
                while ($rlt = mysql_fetch_array($sql, MYSQL_ASSOC)) {
                    $row['student_number'] = $rlt['student_number'];
                }

                $successdata = array('status_code' => "1", 'status' => "success", 'message' => "Login Successful", 'response_code' => "200", 'response_data' => $row);
                $this->response($this->json($successdata), 200);
            } else {
                $error = array('status_code' => "0", 'status' => "error", 'message' => "error while trying to authenticate", 'response_code' => "200", 'student number entered' => $studentNumber);
                $this->response($this->json($error), 200);
            }
        } else {
            $error = array('status_code' => "0", 'status' => "error", 'message' => "validation error", 'response_code' => "200", 'response_data' => $studentNumber);
            $this->response($this->json($error), 200);
        }
    }

    function insertRequestDetails()
    {
        if ($this->get_request_method() != "POST") {
            $error = array('status_code' => "0", 'status' => "error", 'message' => "wrong method", 'response_code' => "406");
            $this->response($this->json($error), 406);
        }

        if (@$_POST['reqparams']) {
            $post = $_POST['reqparams'];

            $name = $post['name'];
            $email = $post['email'];
            $number = $post['number'];
            $site = $post['site'];
            $details = $post['details'];

            $result = mysql_query("INSERT INTO `email_requests` (`name`, `email`, `number`, `site`, `details`) VALUES ('$name', '$email', '$number', '$site', '$details')");
            $lastId = mysql_insert_id($con);
            if (!$result) die('Invalid query: ' . mysql_error());
            $data['message'] = "Added successfully.";
            $data['lastId'] = $lastId;
            $data['responseCode'] = "200";
            $data['status'] = "success";
            print json_encode($data);
        } else {

            $error = array('status_code' => "0", 'status' => "error", 'message' => "Error", 'post error - no reqparams' => "0", 'response_code' => "200");
            $this->response($this->json($error), 200);
        }
    }

    function sendMailEnquiry()
    {

        if (@$_POST['reqparams']) {

            $BlankArray = array();
            $post = $_POST['reqparams'];

            $name = $post['name'];
            $clientEmail = $post['email'];

            $site = $post['site'];

            $number = $post['number'];

            $toemail = FROMEMAIL;

            $message = $name != '' ? "Client Name: " . $name . "\r\n\r\n" : '';
            $message = $message . $number != '' ? "Client Number: " . $number . "\r\n\r\n" : '';
            $message = $message . $site != '' ? "Client Site: " . $site . "\r\n\r\n" : '';
            $message = $message . $post['message'] != '' ?  $post['message'] : '';

            @$send = $this->sendMail($clientEmail, $toemail, $message, $name, APPLICATION_NAME . " - Website God/Godess Enquiry");

            if ($send) {
                $data = array('StatusCode' => "1", 'Status' => "Success", 'Message' => "Email sent.", 'ResponseCode' => "200", 'ResponseData' => $BlankArray);
                $this->response($this->json($data), 200);
            } else {
                $error = array('StatusCode' => "0", 'Status' => "Failed", 'Message' => "Error encountered while sending email.", 'ResponseCode' => "200", 'ResponseData' => $BlankArray);
                $this->response($this->json($error), 200);
            }
        } else {

            $error = array('status_code' => "0", 'status' => "error", 'message' => "Error", 'post error - no reqparams' => "0", 'response_code' => "200");
            $this->response($this->json($error), 200);
        }
    }


    public function sendMail($fromemail, $toemail, $body, $fromname, $subject)
    {
        $headers = "From: " . $fromemail;
        mail($toemail, $subject, $body, $headers);
        return "1";
    }

    public function is_require($a, $i)
    {
        if (!isset($a[$i]) || $a[$i] == '') {
            $message = $i . ' parameter missing or it should not null';
            $data['ResponseData'] = "";
            $data['Message'] = $message;
            $data['ResponseCode'] = "200";
            $data['Status'] = "Failed";
            $data['StatusCode'] = "0";
            print json_encode($data);
            die();
        } elseif ($a[$i] == 'null') {
            $message = $i . ' parameter missing or it should not null';
            $data['ResponseData'] = "";
            $data['Message'] = $message;
            $data['ResponseCode'] = "200";
            $data['Status'] = "Failed";
            $data['StatusCode'] = "0";
            print json_encode($data);
            die();
        } else {
            return $a[$i];
        }
    }
}

$api = new API;
$api->processApi();
