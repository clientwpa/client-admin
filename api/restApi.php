<?php
// include 'libXML.php';
include 'lib.php';
// include '../../PHPMailer/PHPMailerAutoload.php';
include 'config.php';
// include '../LogAPI.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
$_content_type = "application/x-www-form-urlencoded;charset=utf-8";
header("Content-Type:" . $_content_type);
ini_set('max_execution_time', 3000);

/* Store the id number sent by the application */
if ($_POST['Request']) {

    $post = json_decode($_POST['Request']);
    $postArray = array();
    foreach ($post as $key => $field) {
        $postArray[$key] = $field;
    }
    $post = $postArray;
    $ID = @$post['ServiceRequestID'];
} else if ($_GET['Request']) {

    $post = json_decode($_GET['Request']);
    $postArray = array();
    foreach ($post as $key => $field) {
        $postArray[$key] = $field;
    }
    $post = $postArray;
    $ID = @$post['ServiceRequestID'];
} else {

    $incommingContents = trim(file_get_contents("php://input"));
    $post = json_decode($incommingContents, true);

    $postArray = array();
    foreach ($post as $key => $field) {
        $postArray[$key] = $field;
    }
    $post = $postArray;
    $ID = $post['ServiceRequestID'] ? $post['ServiceRequestID'] : '0';
}

$dbDBname = DB;
$dbUsername = DB_USER;
$dbPassword = DB_PASSWORD;
$hostName = DB_SERVER;
$URLName = URL;
$baseUrl = base_url;
$authUser = AUTH_USER;
$authPassword = AUTH_PW;


//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////// MAIN //////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////

switch ($ID) {
    case 1:
        loadUnitInfo($post);
        break;
    case 2:
        GPSLocationUpdate($post);
        break;
    case 3:
        getNextUnitReading($post);
        break;
    case 4;
        updatetNextUnitReading($post);
        break;
    case 5;
        nextUnitReadingNewest($post);
        break;
    case 7;
        unitReadingNewest($post);
        break;
    case 8;
        addFavtoDB($post);
        break;
    case 9;
        deleteFavfromDB($post);
        break;
    case 10;
        loadFavFromDB($post);
        break;
    case 11;
        getAllEnabledUnits($post);
        break;
    default:
        postSent($post);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////// FUNCTIONS /////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
function postSent($post)
{
    return $post;
}
/******************************************************************************************************************/
/* 
        *   ID=1
        *   A function used as a response to ID=1
        *   It is used to countEmailRequest
        *   PARAMETERS: -
        *   Return Value: TBA
        */


// ******************************* LOGIN API *******************************

function getAllEnabledUnits($post)
{
    // print json_encode($post);
    // return;

    $con = connectToDB(); //connect to the DB
    mysql_query('SET NAMES UTF8');

    $sql1 = "SELECT * FROM units WHERE enabled = 1";

    $resultset1 = mysql_query($sql1);

    $rows = array();
    $blankArray = array();
    $rows['Data'] = array();

    while ($row = mysql_fetch_array($resultset1, MYSQL_ASSOC)) {
        $row1 = $row;
        $rows['Data'][] = $row1;
    }

    if ($rows) {
        $data = array('StatusCode' => "1", 'Status' => "Success", 'Message' => "Retrieve Succesful!", 'ResponseCode' => "200", 'ResponseData' => $rows);
    } else {
        $data = array('StatusCode' => "0", 'Status' => "Failed", 'Message' => "Internal Error!", 'ResponseCode' => "200", 'ResponseData' => $blankArray);
    }
    print json_encode($data);
    mysql_close($con);   //close the connection

}

function loadUnitInfo($post)
{
    // print json_encode($post);
    // return;

    $con = connectToDB(); //connect to the DB
    mysql_query('SET NAMES UTF8');

    $locationId = is_require($post, 'location');

    $sql1 = "SELECT * FROM units WHERE location_id = '$locationId'";

    $resultset1 = mysql_query($sql1);

    $rows = array();
    $blankArray = array();
    $rows['Data'] = array();

    while ($row = mysql_fetch_array($resultset1, MYSQL_ASSOC)) {
        $row1 = $row;
        $rows['Data'][] = $row1;
    }

    if ($rows) {
        $data = array('StatusCode' => "1", 'Status' => "Success", 'Message' => "Retrieve Succesful!", 'ResponseCode' => "200", 'ResponseData' => $rows);
    } else {
        $data = array('StatusCode' => "0", 'Status' => "Failed", 'Message' => "Internal Error!", 'ResponseCode' => "200", 'ResponseData' => $blankArray);
    }
    print json_encode($data);
    mysql_close($con);   //close the connection

}

function GPSLocationUpdate($post)
{
    $con = connectToDB(); //connect to the DB
    mysql_query('SET NAMES UTF8');


    $id = !empty($post['id']) ? $post['id'] : '';

    // $unitId = !empty($post['unit_id']) ? $post['unit_id'] : '';
    // $locationId = !empty($post['location_id']) ? $post['location_id'] : '';

    $unitName = !empty($post['unit_name']) ? $post['unit_name'] : '';

    $latitude = !empty($post['unit_latitude']) ? $post['unit_latitude'] : '';
    $longitude = !empty($post['unit_longitude']) ? $post['unit_longitude'] : '';
    // $Address = !empty($post['Address']) ? $post['Address'] : '';

    // $CreatedDateTime = gmdate('Y-m-d H:i:s');
    $ModifyDateTime = gmdate('Y-m-d H:i:s');




    //  if (!empty($OfflineDateTime)) {
    //     $ActualStartLoginTime = gmdate('H:i:s', strtotime($OfflineDateTime));
    // } else {
    //     $ActualStartLoginTime = gmdate('H:i:s');
    // }

    //$ActualStartLoginTime=gmdate('H:i:s');

    $address = '';
    $key = "AIzaSyAxifwnSgSLbU4XFAu-PeKaAO-0v1BWbX8";
    if (empty($post['Address'])) {
        $dataAddr = Get_Address_From_Google_Maps($latitude, $longitude, $key);
        $address = $dataAddr['formatted_address'];
    }

    // print json_encode($post);
    // return;

    // $sql = "insert into units (id,address,unit_id,location_id,unit_name,unit_latitude,unit_longitude,last_used,modified_datetime) values ('" . $Address . "','" . $ActualStartLoginTime . "','" . $LoginType . "','" . $ActualLoginDate . "','" . $OrgID . "','" . $UserID . "','" . $VisitID . "','" . $Latitude . "','" . $Longitude . "','" . $CreatedDateTime . "','" . $ModifyDateTime . "');";

    $sql = "UPDATE `units` SET `unit_name`='$unitName', `unit_latitude`='$latitude', `unit_longitude`='$longitude', `address`='$address', `modified_datetime`='$ModifyDateTime' WHERE `id` = '$id'";

    $result = mysql_query($sql) or die(mysql_error());
    $data = array();
    $BlankArray = array();

    if ($result) {
        $data['ResponseData'] = $address;
        $data['Message'] = "Location Updated";
        $data['ResponseCode'] = "200";
        $data['Status'] = "Success";
        $data['StatusCode'] = "1";
    } else {
        $data['ResponseData'] = $BlankArray;
        $data['Message'] = "Error";
        $data['ResponseCode'] = "200";
        $data['Status'] = "Failed";
        $data['StatusCode'] = "0";
    }
    print json_encode($data);
    mysql_close($con); //close the connection
}

function getNextUnitReading($post)
{
    // print json_encode($post);
    // return;

    $con = connectToDB(); //connect to the DB
    mysql_query('SET NAMES UTF8');

    $unitId = is_require($post, 'unit_id');

    $sql1 = "SELECT * FROM unitReadings WHERE unit_id = '$unitId' ORDER BY Date_and_Time ASC LIMIT 1";

    // --     ORDER BY id DESC LIMIT 10
    // --   ) AS `table` ORDER by id ASC"

    $resultset1 = mysql_query($sql1);

    $rows = array();
    $blankArray = array();
    $rows['Data'] = array();

    while ($row = mysql_fetch_array($resultset1, MYSQL_ASSOC)) {
        $row1 = $row;
        $rows['Data'][] = $row1;
    }

    if ($rows) {
        $data = array('StatusCode' => "1", 'Status' => "Success", 'Message' => "Retrieve Succesful!", 'ResponseCode' => "200", 'ResponseData' => $rows);
    } else {
        $data = array('StatusCode' => "0", 'Status' => "Failed", 'Message' => "Internal Error!", 'ResponseCode' => "200", 'ResponseData' => $blankArray);
    }
    print json_encode($data);
    mysql_close($con);   //close the connection

}

function nextUnitReadingNewest($post)
{
    // print json_encode($post);
    // return;

    $con = connectToDB(); //connect to the DB
    mysql_query('SET NAMES UTF8');

    $unitId = is_require($post, 'unit_id');

    $sql1 = "SELECT * FROM unitReadings WHERE unit_id = '$unitId' ORDER BY Date_and_Time DESC LIMIT 1";

    // --     ORDER BY id DESC LIMIT 10
    // --   ) AS `table` ORDER by id ASC"

    $resultset1 = mysql_query($sql1);

    $rows = array();
    $blankArray = array();
    $rows['Data'] = array();

    while ($row = mysql_fetch_array($resultset1, MYSQL_ASSOC)) {
        $row1 = $row;
        $rows['Data'][] = $row1;
    }

    if ($rows) {
        $data = array('StatusCode' => "1", 'Status' => "Success", 'Message' => "Retrieve Succesful!", 'ResponseCode' => "200", 'ResponseData' => $rows);
    } else {
        $data = array('StatusCode' => "0", 'Status' => "Failed", 'Message' => "Internal Error!", 'ResponseCode' => "200", 'ResponseData' => $blankArray);
    }
    print json_encode($data);
    mysql_close($con);   //close the connection

}

function unitReadingNewest($post)
{
    // print json_encode($post);
    // return;

    $con = connectToDB(); //connect to the DB
    mysql_query('SET NAMES UTF8');

    $unitId = is_require($post, 'unit_id');

    $sql1 = "SELECT * FROM unitReadings WHERE unit_id = '$unitId' ORDER BY Date_and_Time DESC LIMIT 50";
    $resultset1 = mysql_query($sql1);

    $rows = array();
    $blankArray = array();
    $rows['Data'] = array();

    while ($row = mysql_fetch_array($resultset1, MYSQL_ASSOC)) {
        $row1 = $row;
        $rows['Data'][] = $row1;
    }

    if ($rows) {
        $data = array('StatusCode' => "1", 'Status' => "Success", 'Message' => "Retrieve Succesful!", 'ResponseCode' => "200", 'ResponseData' => $rows);
    } else {
        $data = array('StatusCode' => "0", 'Status' => "Failed", 'Message' => "Internal Error!", 'ResponseCode' => "200", 'ResponseData' => $blankArray);
    }
    print json_encode($data);
    mysql_close($con);   //close the connection

}

function updatetNextUnitReading($post)
{
    // print json_encode($post);
    // return;

    $con = connectToDB(); //connect to the DB
    mysql_query('SET NAMES UTF8');

    $id = is_require($post, 'id');

    $dateTime = gmdate('Y-m-d H:i:s');

    $sql = "UPDATE `unitReadings` SET `Date_and_Time`='$dateTime' WHERE `id` = '$id'";

    $result = mysql_query($sql) or die(mysql_error());
    // $last_insert_id = mysql_insert_id();
    $data = array();
    $BlankArray = array();

    if ($result) {
        $data['ResponseData'] = $dateTime;
        $data['Message'] = "Updated Success";
        $data['ResponseCode'] = "200";
        $data['Status'] = "Success";
        $data['StatusCode'] = "1";
    } else {
        $data['ResponseData'] = $BlankArray;
        $data['Message'] = "Error";
        $data['ResponseCode'] = "200";
        $data['Status'] = "Failed";
        $data['StatusCode'] = "0";
    }
    print json_encode($data);
    mysql_close($con);
}

/* SYSTEM FUNCTIONS */

function is_require($a, $i)
{
    if (!isset($a[$i]) || $a[$i] == '') {
        $message = $i . ' parameter missing or it should not null';
        $data['ResponseData'] = "";
        $data['Message'] = $message;
        $data['ResponseCode'] = "200";
        $data['Status'] = "Failed";
        $data['StatusCode'] = "0";
        print json_encode($data);
        die();
    } elseif ($a[$i] == 'null') {
        $message = $i . ' parameter missing or it should not null';
        $data['ResponseData'] = "";
        $data['Message'] = $message;
        $data['ResponseCode'] = "200";
        $data['Status'] = "Failed";
        $data['StatusCode'] = "0";
        print json_encode($data);
        die();
    } else {
        return $a[$i];
    }
}

function Get_Address_From_Google_Maps($lat, $lon, $key)
{

    $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lon&sensor=false&key=$key";

    // Make the HTTP request
    $data = @file_get_contents($url);
    // Parse the json response
    $jsondata = json_decode($data, true);

    // If the json data is invalid, return empty array

    if (!check_status($jsondata)) {
        return array();
    }

    $address = array(
        'formatted_address' => google_getAddress($jsondata),
    );

    return $address;
}

function loadFavFromDB($post)
{

    // print json_encode($post);
    // mysql_close($con);
    // return;

    $con = connectToDB(); //connect to the DB
    mysql_query('SET NAMES UTF8');

    $userId = is_require($post, 'user_id');

    $sql = "SELECT * FROM favs_tb WHERE user_id = '$userId'";
    $result = mysql_query($sql) or die(mysql_error());

    $rows = array();
    while ($row = mysql_fetch_assoc($result)) {
        $rows[] = $row;
    }
    $data = array();
    $BlankArray = array();
    if ($rows) {
        $data['ResponseData'] = $rows;
        $data['Message'] = "Fav list selected successfully";
        $data['ResponseCode'] = "200";
        $data['Status'] = "Success";
        $data['StatusCode'] = "1";
    } else {
        $data['ResponseData'] = $BlankArray;
        $data['Message'] = "Error selecting Fav list";
        $data['ResponseCode'] = "200";
        $data['Status'] = "Failed";
        $data['StatusCode'] = "0";
    }

    print json_encode($data);
    mysql_close($con); //close the connection

}

function addFavtoDB($post)
{

    // print json_encode($post);
    // mysql_close($con);
    // return;

    $con = connectToDB(); //connect to the DB
    mysql_query('SET NAMES UTF8');

    $unitId = is_require($post, 'unit_id');
    $userId = is_require($post, 'user_id');
    $locationId = is_require($post, 'location_id');
    $unitName = is_require($post, 'unit_name');
    $unitAddress = $post['unit_location'];


    $sql1 = "SELECT * FROM favs_tb WHERE unit_id = '$unitId' AND user_id = '$userId'";
    $userFavListData = mysql_query($sql1);

    $data = array();
    if (mysql_num_rows($userFavListData) > 0) {
        $message = 'Unit Already in Favourites List - Unit ID : ' . $unitId;

        $data['ResponseData'] = false;
        $data['Message'] = $message;
        $data['ResponseCode'] = "200";
        $data['Status'] = "Success";
        $data['StatusCode'] = "1";
    } else {

        $sql = "insert into favs_tb (unit_id,user_id,location_id,unit_name,unit_location) values ('" . $unitId . "','" . $userId . "','" . $locationId . "','" . $unitName . "','" . $unitAddress . "');";

        $message = 'Unit Added to Favourites List - Unit ID : ' . $unitId;
        $result = mysql_query($sql) or die(mysql_error());

        if ($result) {
            $data['ResponseData'] = true;
            $data['Message'] = $message;
            $data['ResponseCode'] = "200";
            $data['Status'] = "Success";
            $data['StatusCode'] = "1";
        } else {
            $data['ResponseData'] = 'error';
            $data['Message'] = "Error";
            $data['ResponseCode'] = "200";
            $data['Status'] = "Failed";
            $data['StatusCode'] = "0";
        }
    }
    print json_encode($data);
    mysql_close($con); //close the connection

}

function deleteFavfromDB($post)
{
    $con = connectToDB(); //connect to the DB
    mysql_query('SET NAMES UTF8');

    $unitId = is_require($post, 'unit_id');
    $userId = is_require($post, 'user_id');

    $sql1 = "SELECT * FROM favs_tb WHERE unit_id = '$unitId' AND user_id = '$userId'";

    $userFavListData = mysql_query($sql1);

    // print json_encode($post);
    // mysql_close($con);
    // return;


    $data = array();
    if (mysql_num_rows($userFavListData) > 0) {

        $sql = "DELETE FROM favs_tb WHERE unit_id = '$unitId' AND user_id = '$userId'";

        $result = mysql_query($sql) or die(mysql_error());

        $message = 'Unit Removed from Favourites List - Unit ID : ' . $unitId;

        if ($result) {
            $data['ResponseData'] = true;
            $data['Message'] = $message;
            $data['ResponseCode'] = "200";
            $data['Status'] = "Success";
            $data['StatusCode'] = "1";
        } else {
            $data['ResponseData'] = 'error';
            $data['Message'] = "Error";
            $data['ResponseCode'] = "200";
            $data['Status'] = "Failed";
            $data['StatusCode'] = "0";
        }
    } else {

        $message = 'Unit Not in Favourites List - Unit ID : ' . $unitId;

        $data['ResponseData'] = false;
        $data['Message'] = $message;
        $data['ResponseCode'] = "200";
        $data['Status'] = "Success";
        $data['StatusCode'] = "1";
    }
    print json_encode($data);
    mysql_close($con); //close the connection
}

function google_getAddress($jsondata)
{
    return $jsondata["results"][0]["formatted_address"];
}

function check_status($jsondata)
{
    if ($jsondata["status"] == "OK") return true;
    return false;
}

function checkRequest()
{
    echo 'Wrong Method';
}

function getBrowser()
{
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version = "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    } elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }

    // Next get the name of the useragent yes seperately and for good reason
    if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    } elseif (preg_match('/Firefox/i', $u_agent)) {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    } elseif (preg_match('/OPR/i', $u_agent)) {
        $bname = 'Opera';
        $ub = "Opera";
    } elseif (preg_match('/Chrome/i', $u_agent)) {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    } elseif (preg_match('/Safari/i', $u_agent)) {
        $bname = 'Apple Safari';
        $ub = "Safari";
    } elseif (preg_match('/Netscape/i', $u_agent)) {
        $bname = 'Netscape';
        $ub = "Netscape";
    }

    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }

    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
            $version = $matches['version'][0];
        } else {
            $version = $matches['version'][1];
        }
    } else {
        $version = $matches['version'][0];
    }

    // check if we have a number
    if ($version == null || $version == "") {
        $version = "?";
    }

    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
}
