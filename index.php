<!DOCTYPE html>
<html lang="en" ng-app="myApp">

<head>

  <base href="https://rejuvenair.app/" />

  <link rel="manifest" href="manifest.json">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="application-name" content="Rejuvenair">
  <meta name="apple-mobile-web-app-title" content="Rejuvenair">

  <link rel="manifest" href="manifest.json">

  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">

  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

  <!-- <meta name="msapplication-TileImage" content="img/icons/msapplication-icon-144x144.png"> -->
  <meta name="msapplication-TileColor" content="#000000">

  <!-- icons -->
  <link rel="apple-touch-icon" href="img/icons/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="152x152" href="img/icons/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="img/icons/apple-touch-icon-180x180.png">
  <link rel="apple-touch-icon" sizes="120x120" href="img/icons/apple-touch-icon-120x120.png">

  <meta name="msapplication-starturl" content="/">

  <!-- Fav-Icons -->
  <link href="favicon.ico" rel="shortcut icon">
  
  <link rel="icon" type="image/png" href="icon-16x16.png" sizes="16x16">
  <link rel="icon" type="image/png" href="icon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="icon-96x96.png" sizes="96x96">

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"">

  <script src=" js/jquery-3.1.0.min.js">
  </script>

  <!-- stars make local resources -->

  <!-- <script src="https://code.highcharts.com/highcharts.js"></script> -->

  <!--    <link rel="shortcut icon" href="images/favicon.png"> -->

  <link rel="stylesheet" href="css/switch-button.css">
  <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>

  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- bootstrap theme -->
  <link href="css/bootstrap-theme.css" rel="stylesheet">
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link href="css/toaster.css" rel="stylesheet">

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet">

  <link href="css/style.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
  <link rel="stylesheet" href="assets/alertifyjs/ng-alertify.css" />


<!-- mobile-web-app-capable
apple-mobile-web-app-capable
application-name
apple-mobile-web-app-title
apple-mobile-web-app-status-bar-style
apple-touch-icon -->

  <script>
    $(window).on('load', function() {
      // will first fade out the loading animation
      $("#status").fadeOut();
      // will fade out the whole DIV that covers the website.
      $("#preloader").delay(100).fadeOut("slow");
    });
  </script>

</head>

<body ng-cloak="" scroll="no">

  <!-- <div id="preloader">
      <div id="status">&nbsp;</div>
    </div>
     -->

  <div class="containers">

    <div data-ng-view="" id="ng-view" class="slide-animation"></div>

  </div>


</body>

<toaster-container class="text-center" toaster-options="{'time-out': 3000}"></toaster-container>

<!-- javascripts -->


<script src="js/angular.min.js"></script>
<script src="js/angular-sanitize.min.js"></script>
<script src="js/angular-route.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-touch.min.js"></script>

<script type="text/javascript" src="js/angular-validator.min.js"></script>
<script type="text/javascript" src="js/angular-validator-rules.min.js"></script>
<script type="text/javascript" src="js/toaster.js"></script>
<script type="text/javascript" src="js/sweetalert2.all.min.js"></script>
<script src="assets/alertifyjs/ng-alertify.js"></script>


<!-- <script type="text/javascript" src="js/globe.js"></script> -->

<script src="app/app.js"></script>
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
<script src="app/homeCtrl.js"></script>
<script src="app/moreInfoCtrl.js"></script>
<script src="app/aboutCtrl.js"></script>
<script src="app/authCtrl.js"></script>
<script src="app/favCtrl.js"></script>
<script src="app/simCtrl.js"></script>
<script src="app/historyCtrl.js"></script>
<script src="app/sideMenuCtrl.js"></script>
<script src="app/gpsCtrl.js"></script>
<script src="app/gpsNearMeCtrl.js"></script>

<script language="javascript" type="text/javascript" src="libraries/p5.min.js"></script>
<script language="javascript" type="text/javascript" src="libraries/p5.ble.js"></script>
<script language="javascript" type="text/javascript" src="js/bluetooth.js"></script>
<script language="javascript" type="text/javascript" src="js/drawScreen.js"></script>
<script language="javascript" type="text/javascript" src="js/JulianChromeApp.js"></script>


<script src="js/p5.dom.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.7.2/addons/p5.dom.min.js"></script>

<script src="js/bootstrap_4.5.0.min.js"></script>

<script src="js/jquery-ui-1.9.2.custom.min.js"></script>

<script language="javascript" type="text/javascript" src="js/highcharts.js"></script>

<script src="js/tween-max.js"></script>
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.14/angular-animate.min.js"></script> -->
<!-- <script src="//code.angularjs.org/1.2.9/angular-touch.min.js"></script> -->

<!-- nice scroll -->
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jquery.nicescroll.js" type="text/javascript"></script>

<script src="js/bootstrap-datepicker.js"></script>
<!--   <script src="js/moment-min.js"></script> -->


<script>
  // makes sure the whole site is loaded
</script>


<script type="text/javascript" src="js/angular-validator.min.js"></script>
<script type="text/javascript" src="js/angular-validator-rules.min.js"></script>
<script src="js/ui-bootstrap-tpls.js"></script>

<script src="js/angular-ui-bootstrap-modal.js"></script>


<script type="text/javascript">
  if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
      navigator.serviceWorker.register('service-worker.js').then(function(registration) {
        // Registration was successful
        console.log('ServiceWorker registration successful with scope: ', registration.scope);
      }, function(err) {
        // registration failed :(
        console.log('ServiceWorker registration failed: ', err);
      });
    });
  }

  function getfocus() {
    //document.getElementById("fname").focus();
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  function getfocusscroll() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }
</script>

</html>